<button type="button" class="btn-circle-gen btn-close-modal" data-dismiss="modal" aria-label="Close">
	<i class="fa fa-times" aria-hidden="true"></i>
</button>

<p class="txt-title-modal">¡Ya participaste!</p>

<p class="txt-instruction-modal">regresa mañana :)</p>

<!-- 
<div class="animated infinite pulse">
	<a href="index.php">
		<button class="button-gen-send btn-send" type="submit">
			<div class="box-shadow-inset"></div>
			<p>aquí <i class="fas fa-play-circle"></i></p>
		</button>
	</a>
</div> -->

<div class="footer-modal">
	<?php include("social.php"); ?>
</div>
