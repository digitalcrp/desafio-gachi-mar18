<div class="wrapp-tyc">

	<div class="center title-tyc">
		“El desafío Oh My Gachi!”
	</div>

	<p class="center">
		<strong>Planeta, tu música en inglés te da la posibilidad de conocer a Gachi y ganar grandes premios. Participa y podrás ser unos de los 2 ganadores que conocerá a Gachi, junto con un acompañante, para compartir con ella un almuerzo a realizarse el 20 de abril en Pizza Rock</strong>
	</p>

	<br>

	<div class="subtitle-tyc">
		<strong>1. Para participar:</strong>
	</div>

	<p class="tab">
		Ingresa a la web de Radio Planeta  <a href="https://planeta.pe/" target="_blank" style="color: #ff00d2;">(planeta.pe)</a>, busca la imagen del concurso, dale clic a esta y regístrate completando un formulario con tus datos personales (nombres, apellidos, correo electrónico, DNI, teléfono, sexo y fecha de nacimiento).
	</p>

	<p class="tab">Con este registro obtendrás 1 punto.</p>

	<p class="tab">Además, podrás obtener más puntos a través de los retos que se darán durante 3 etapas:</p>

	<ul class="tab-ne">
		<li><strong>Etapa 1:</strong> Del 21 de marzo, hasta las 11 a.m. del día 28 de marzo. En esta etapa podrás responder trivias (podrás responder 5 preguntas por día). Por cada pregunta respondida correctamente, obtendrás 1 punto. En esta etapa podrás llegar a acumular hasta 40 puntos como máximo.</li>

		<li><strong>Etapa 2:</strong> Desde las 12:00 p.m. del día 28 de marzo, hasta las 11:00 a.m. del día 4 de abril. En esta etapa podrás adivinar las dos canciones que Gachi tarareará en los videos que subiremos (podrás adivinar 2 canciones por día). Por cada canción adivinada correctamente obtendrás 1 punto. En esta etapa podrás llegar a acumular hasta 16 puntos como máximo. </li>


		<li><strong>Etapa 3:</strong> Desde las 12:00 p.m. del día 4 de abril,  hasta las 12:00 p.m. del día 17 de abril. En esta etapa podrás realizar un dibujo donde aparezca Gachi y subirlo en el mini sitio del concurso. Gachi elegirá sus 10 dibujos favoritos (5 dibujos por semana). Los primeros 5 dibujos serán publicados el 11 de abril y los otros 5 dibujos el 17 de abril. Durante toda esta etapa, el participante solo podrá subir un dibujo y acumulará 50 puntos.</li>
	</ul>


	<p class="tab">Cada punto equivale a un “ticket en el ánfora virtual” para el sorteo. Es decir, cada punto se registrará en la base de datos, por lo tanto, a más puntos, más oportunidades de ganar.</p>


	<div class="subtitle-tyc tab">
		Nota:
	</div>

	<ul class="tab-ne">
		<li>Podrás registrarte en cualquier momento durante la promoción.</li>

		<li>El participante que no proporcione todos los datos personales solicitados y/o éstosno sean actuales, reales, correctos y verdaderos podrá ser descalificado en cualquier momento de la promoción, no pudiendo ser en consecuencia acreedor de ningún premio.</li>

		<li>En caso de detectarse actividad anormal relacionada con el uso del formulario de participación y/o con la generación de puntos, se penalizará a dicho participante cancelando su registro, eliminándolo del concurso sin previo aviso y bloqueando su participación en el resto de la campaña, no pudiendo ser acreedor de ningún premio.</li>

		<li>Respecto del dibujo, con tu participación y puesta a disposición de tu dibujo, otorgas automáticamente de forma gratuita y exclusiva a CRP Medios y Entretenimiento S.A.C. (en adelante, CRP) tu consentimiento para la utilización, difusión, edición, sincronización, modificación, transformación, copia, reproducción, adaptación, realización de arreglos y comunicación pública de tu dibujo, en forma parcial o total, para los fines de este concurso por tiempo indefinido, para el Perú y en el resto de países del mundo, sin derecho a percibir ninguna remuneración.</li>

		<li>La participación en la promoción constituye aceptación plena y sin reservas de cada uno de los términos y condiciones generales establecidas en el presente documento.</li>
		
	</ul>

	<div class="subtitle-tyc">
		<strong>2. Ámbito  de la promoción:</strong>
	</div>
	<p class="tab">
		Esta promoción es válida a nivel Lima Metropolitana y Callao.
	</p>

	<div class="subtitle-tyc">
		<strong>3. Vigencia:</strong>
	</div>
	<p class="tab">
		Del miércoles 21 de marzo del 2018 hasta el 17 de abril del 2018.
	</p>

	<div class="subtitle-tyc">
		<strong>4. Fecha del sorteo:</strong>
	</div>
	<p class="tab">
		Martes 18 de abril del 2018 a la 03:00 p.m.
	</p>

	<div class="subtitle-tyc">
		<strong>5. Comunicación de resultados:</strong>
	</div>
	<p class="tab">
		La relación con los 2 ganadores será publicada el miércoles 18 de abril del 2018 a partir de las 5:00 p.m. en la sección de concursos de la web de Radio Planeta (planeta.pe). Además, la lista de ganadores podrá ser difundida durante la programación de la radio. No se llamará telefónicamente a ningún ganador. 
	</p>

	<div class="subtitle-tyc">
		<strong>6. Premios (Stock):</strong>
	</div>

	<ul class="tab-ne">
		<li>
			<strong>Primer ganador:</strong><br>
			o	1 almuerzo para el ganador y un acompañante con Gachi Rivero.*<br>
			o	1 gopro hero 4 session<br>
			o	1 CD ‘Fifth Harmony’ de Fifth Harmony<br>
			o	1 CD ‘Funk Wav Bounces vol. 1’ de Calvin Harris.<br>
		</li>
		<br>

		<li>
			<strong>Segundo ganador:</strong><br>
			o	1 almuerzo para el ganador y un acompañante con Gachi Rivero.*<br>
			o	1 audífono Uproar Wireless Skullcandy<br>
			o	1 CD ‘DUA LIPA’ de Dua Lipa<br>
			o	1 CD Funk Wav Bounces vol. 1’ de Calvin Harris.<br>
		</li>
	</ul>

	<p class="tab">* Se realizará un único almuerzo en presencia de Gachi, de los dos ganadores del concurso, sus acompañantes y representantes de nuestra empresa. Si el ganador es menor de edad, el acompañante necesariamente deberá ser su padre y/o madre y/o tutor legal. El almuerzo se realizará en el restaurante Pizza Rock, ubicado en Calle Ignacio Merino 250 - Miraflores, el día 20 de abril del 2018 desde las 3 p.m. hasta las 6 p.m. En el restaurante se entregará a los ganadores una carta con opciones para que puedan escoger de entre dichas opciones una pizza slice gigante (a elección entre las variedades disponibles), un postre (a elección entre las variedades disponibles) y un Fun Drink (a elección entre las variedades disponibles).</p>


	<div class="subtitle-tyc">
		<strong>7. Entrega de premios:</strong>
	</div>

	<p class="tab">El 20 de abril del 2018 los dos ganadores y sus acompañantes deberán presentarse en el restaurante Pizza Rock, ubicado en Calle Ignacio Merino 250 – Miraflores, a las 2:30 p.m. (hora exacta). Si el ganador es menor de edad, el acompañante necesariamente deberá ser su padre y/o madre y/o tutor legal.</p>

	<p class="tab">Los ganadores deberán presentarse con su DNI para poder reclamar su premio. El premio es intransferible y no canjeable.</p> 

	<p class="tab">La radio no asumirá ningún gasto (sea por transporte, alojamiento, etc.) para el recojo del premio ni para el disfrute del premio.
	</p>

	<p class="tab">En caso que a criterio de nuestra empresa se detecte algún caso de intento de suplantación de identidad (o que una persona intente hacerse pasar por algún ganador) y/o que se han brindado datos falsos ya sea al momento de participar o al momento de reclamar y/o disfrutar su premio, se iniciarán las acciones legales que correspondan (incluso penales) en contra de dichas personas.</p>




	<div class="subtitle-tyc">
		<strong>8. Otras disposiciones:</strong>
	</div>
	<p class="tab">
		Para cualquier cuestión judicial que pudiera derivarse de la realización de este concurso, los participantes y nuestra empresa se someterán a la jurisdicción de los tribunales ordinarios de la ciudad de Lima Metropolitana, renunciando el fuero de los domicilios que les pudieran corresponder.
	</p>

	<p class="tab"> Nuestra empresa podrá modificar los Términos y Condiciones de este concurso cuando circunstancias no previstas lo justifiquen, siempre y cuando las mismas no alteren la esencia de la promoción. La participación en este concurso implica la aceptación de estos Términos y Condiciones, así como de las decisiones que adopte nuestra empresa, conforme a derecho, sobre cualquier cuestión no prevista en ellas.</p>
</div>