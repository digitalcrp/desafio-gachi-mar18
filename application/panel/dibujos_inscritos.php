<?php
include_once("../config/conn.php");

?>


<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Moderador CRP</title>

    <!-- Bootstrap core CSS -->
    <link href="css/admin_menu.css?v=4" rel="stylesheet">
    <script type="text/javascript" src="../../assets/js/jquery-1.12.4.min.js"></script>

    <?php
    // SELECT count(id_resultados_inscrito), pregunta, respuesta, fecha FROM `resultados_inscritos` WHERE campania_id = 376 group by categoria_id, multimedia_id, fecha
    $query_dibujos = "select texto, fecha, dato3, id_adicional, inscrito_id, estado FROM adicionales WHERE campania_id = 376 and numero = 3";
    $result_dibujos = mysql_query($query_dibujos);
 ?>
  </head>

  <body>

  <div class="row contenedor">
  
    <center><h2>Panel del Desafío de Gachi</h2></center>

    <nav>
      <ul>
        <li><a href="index.php">Lista de inscritos diarios</a></li>
        <li><a href="jugadas_diarias.php">Lista de jugadas diarias</a></li>
        <li><a href="puntaje_inscritos.php">Puntaje de inscritos</a></li>
        <li><a href="puntaje_inscritos_dia.php">Puntaje de inscritos por día</a></li>
        <li><a href="puntaje_inscritos_dia_tipo.php">Filtro de inscritos por día</a></li>
        <li><a href="dibujos_inscritos.php">Dibujos Etapa 3</a></li>

      </ul>

    </nav>
    <br><br>
        
    <div id="container" class="container-fluit">  
      <div class="col-md-12">
         <table class="table table-striped table-bordered" id="listimages"> 
              <thead> 
                  <tr> 
                      <th>Id Inscrito:</th> 
                      <th>Nombre:</th> 
                      <th>Fecha:</th>  
                      <th>Imagen:</th>
                      <th>estado</th>
                      <th>Acción:</th>
                  </tr> 
              </thead> 
              <tbody> 
                <?php 
                while ($row_dibujos = mysql_fetch_array($result_dibujos)){ 
                    switch ($row_dibujos['estado']) {
                      case 2:
                      case 0:
                        $estado = "no publicado";
                        $action = "aprobar";
                        break;

                      case 1:
                      default:
                        $estado = "publicado";
                        $action = "desaprobar";

                        break;
                      
                    }
                    ?>
                  <tr> 
                      <td><?php echo $row_dibujos['inscrito_id']; ?></td>                
                      <td><?php echo $row_dibujos['dato3']; ?></td>        
                      <td><?php echo $row_dibujos['fecha']; ?></td>        
                      <td><img src="<?php echo $row_dibujos['texto']; ?>" width="100" ></td>   
                      <td><?php echo $estado; ?></td> 
                      <td><a href="action.php?action=<?php echo $action; ?>&id=<?php echo $row_dibujos['id_adicional']; ?>"><?php echo $action; ?></a></td> 

                  </tr>   
                <?php } ?>                
             </tbody> 
          </table>
      </div>     

       <!--div para el fondo de pantalla-->
    <div id='background'></div>
    <!--div para visualizar la imagen grande con el boton cerrar-->
    <div id='preview'><div id='close'></div><div id='content'></div></div>

    </div>  

    <br>
    
  </div> 

<style type="text/css">
  /*Estilo para el fondo de pantalla cuando estamos mostrando la imagen grande*/
#background {
    display:none;
    position: absolute;
    top:0px;left:0px;width:100%;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
    filter: alpha(opacity=80);
    -moz-opacity: 0.8;
    -khtml-opacity: 0.8;
    opacity: 0.8;
   /* background-color: #000;*/
    z-index: 999;
}

/*Estilo para la capa que contendra la imagen grande y la cruz de cerrar*/
#preview {
    display: none;
    position: absolute;
    /*min-width: 25%;
    min-height: 25%;*/
    border: 1px solid #D8D7D8;
    /*background-color: #FFF;*/
    box-shadow: 1px 1px 5px #DDD;
    padding: 5px;
    z-index: 2;
        top: 0;
    left: 0;
    /*display: block;*/
    bottom: 0;
    right: 0;
    background: rgba(0,0,0,0.6);
    height: 100%;
}

/*Estilo para el contenedor de la imagen grande*/
#content {
   /* width:100%;
    height:100%;*/
    display: table;
    margin: 50px auto 0;
    padding: 0 300px;
}

/*Estilo para el boton cerrar*/
#close {
    position: absolute;
    border:0px solid;
    width:16px;
    height:16px;
    right:2px;
    top:2px;
    background:url('close.png');
}

.url {text-align:center;margin-top:20px;}

</style>
<script type="text/javascript">
  /**
 * http://www.lawebdelprogramador.com
 */
$(document).ready(function(){
    // Funcion que se ejecuta al hacer click sobre una imagen
    $("#listimages img").click(function(){
        // Posicionamos las capas
        $('#background').css('height',$(document).height());
        $(document).scrollTop(0);
        // $('#preview').css('top',(($(window).height()/4) - ($('#preview').height()/4) + $(document).scrollTop()));
        // $('#preview').css('left', ($(document).width()/2) - ($('#preview').width()/2));
        // Cargamos la imagen en la capa grande
        $('#content').html("<img src='"+$(this).attr("src")+"' width='100%'>");
        // Mostramos las capas
        $('#preview').fadeIn();
        $('#background').fadeIn();
    });
    
    // Cerramos las capas al pulsar sobre el fondo
    $("#background").click(function(){
        $('#background').fadeOut();
        $('#preview').fadeOut();
    });
    // Cerramos las capas al pulsar sobre la cruz
    $("#close").click(function(){
        $('#background').fadeOut();
        $('#preview').fadeOut();
    });
});

</script>

</body></html>