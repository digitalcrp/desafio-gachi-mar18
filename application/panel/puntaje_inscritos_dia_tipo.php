<?php
include_once("../config/tools.php");
$tools = new Tools();

?>


<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Moderador CRP</title>

    <!-- Bootstrap core CSS -->
    <link href="css/admin_menu.css?v=4" rel="stylesheet">

    <?php

      $query = '';
      if(!empty($_POST['fecha_inicio']))
      {
        $fecha_inicio = $_POST['fecha_inicio'];
        $query = ' AND fecha >= "'.$fecha_inicio.'"';
      }

       if(!empty($_POST['fecha_fin']))
      {
        $fecha_fin = $_POST['fecha_fin'];
        $query .= ' AND fecha <= "'.$fecha_fin.'"';
      }

    $query_inscritos_puntaje = "select sum(ri.acierto) as puntaje, ri.inscrito_id, i.nombre, i.ap_paterno, i.email, ri.tipo, ri.fecha FROM resultados_inscritos as ri inner join inscritos i on i.id_inscrito = ri.inscrito_id WHERE ri.campania_id = 376 ".$query." GROUP by ri.fecha, ri.inscrito_id ";
    // echo $query_inscritos_puntaje ;
    $result_inscritos_puntaje = mysql_query($query_inscritos_puntaje);


 
    ?>
  </head>

  <body>

  <div class="row contenedor">
  
    <center><h2>Panel del Desafío de Gachi</h2></center>

    <nav>
      <ul>
        <li><a href="index.php">Lista de inscritos diarios</a></li>
        <li><a href="jugadas_diarias.php">Lista de jugadas diarias</a></li>
        <li><a href="puntaje_inscritos.php">Puntaje de inscritos</a></li>
        <li><a href="puntaje_inscritos_dia.php">Puntaje de inscritos por día</a></li>
        <li><a href="puntaje_inscritos_dia_tipo.php">Filtro de inscritos por día</a></li>
        <li><a href="dibujos_inscritos.php">Dibujos Etapa 3</a></li>

      </ul>

    </nav>
    <br><br>
    
    <div class="container">
      <div style="text-align: left;font-size: 18px;">Filtro de inscritos por día</div>       
    </div>
    <br>

    <div> 
    <form action="puntaje_inscritos_dia_tipo.php" method="post">  
        <table>
          <tr>
            <td>Fecha inicio</td>
            <td><input type="date" name="fecha_inicio" value="<?php echo $fecha_inicio; ?>"></td>
          </tr>
          <tr>
            <td>Fecha fin</td>
            <td><input type="date" name="fecha_fin" value="<?php echo $fecha_fin; ?>"></td>
          </tr>
          <tr>
          <td colspan="2"><button>Buscar</button></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
    </form>
    </div>
    
    <div id="container" class="container-fluit">  
      <div class="col-md-12">
         <table class="table table-striped table-bordered"> 
              <thead> 
                  <tr> 
                      <th>Id:</th>  
                      <th>Nombre:</th>
                      <th>Apellido:</th>
                      <th>Email:</th>
                      <th>Fecha:</th>
                      <th>Etapa:</th>
                      <th>Puntaje:</th>
                  </tr> 
              </thead> 
              <tbody> 
                <?php 
               
                while ($row_inscritos_puntaje = mysql_fetch_array($result_inscritos_puntaje)) { 
                  ?>
                  <tr> 
                      <td><?php echo $row_inscritos_puntaje['inscrito_id']; ?></td>                
                      <td><?php echo utf8_encode($row_inscritos_puntaje['nombre']); ?></td>   
                      <td><?php echo utf8_encode($row_inscritos_puntaje['ap_paterno']); ?></td>          
                      <td><?php echo $row_inscritos_puntaje['email']; ?></td> 
                      <td><?php echo $row_inscritos_puntaje['fecha']; ?></td>
                      <td><?php echo $row_inscritos_puntaje['tipo']; ?></td>          
                      <td><?php echo $row_inscritos_puntaje['puntaje']; ?></td>             
                  </tr>   
                <?php } ?>          
             </tbody> 
          </table>
      </div>     
    </div>  

  </div> 


</body></html>