<?php
include_once("../config/tools.php");
$tools = new Tools();

?>


<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Moderador CRP</title>

    <!-- Bootstrap core CSS -->
    <link href="css/admin_menu.css?v=4" rel="stylesheet">

    <?php
    // SELECT count(id_resultados_inscrito), pregunta, respuesta, fecha FROM `resultados_inscritos` WHERE campania_id = 376 group by categoria_id, multimedia_id, fecha
    $query_votos = "select count(inscrito_id) as cantidad, fecha FROM votaciones_desafio_2018 WHERE campania_id = 376
group by fecha";
    $result_votos = mysql_query($query_votos);
 
    ?>
  </head>

  <body>

  <div class="row contenedor">
  
    <center><h2>Panel del Desafío de Gachi</h2></center>

    <nav>
      <ul>
        <li><a href="index.php">Lista de inscritos diarios</a></li>
        <li><a href="jugadas_diarias.php">Lista de jugadas diarias</a></li>
        <li><a href="puntaje_inscritos.php">Puntaje de inscritos</a></li>
        <li><a href="puntaje_inscritos_dia.php">Puntaje de inscritos por día</a></li>
        <li><a href="puntaje_inscritos_dia_tipo.php">Filtro de inscritos por día</a></li>
        <li><a href="dibujos_inscritos.php">Dibujos Etapa 3</a></li>
      </ul>

    </nav>
    <br><br>
    
    <div class="container">
      <div style="text-align: left;font-size: 18px;">Lista de jugadas diarias</div>       
    </div>
    <br>
    
    <div id="container" class="container-fluit">  
      <div class="col-md-12">
         <table class="table table-striped table-bordered"> 
              <thead> 
                  <tr> 
                      <th>Fecha:</th>  
                      <th>Cantidad:</th>
                  </tr> 
              </thead> 
              <tbody> 
                <?php 
                $cantidad_votos = 0;
                while ($row_votos = mysql_fetch_array($result_votos)){
                    $votos_diarios = intval($row_votos['cantidad']/5);
                    $cantidad_votos += intval($votos_diarios);
                  ?>
                  <tr> 
                      <th><?php echo $row_votos['fecha']; ?></th>                
                      <td><?php echo $votos_diarios; ?></td>      
                  </tr>   
                <?php } ?>              
                <tr><td colspan="2"></td></tr>
                <tr><th>TOTAL:</th><td><strong><?php echo $cantidad_votos; ?></strong></td></tr>      
             </tbody> 
          </table>
      </div>     
    </div>  

  </div> 


</body></html>