<?php
require_once("conn.php");
require_once("encode.php");

class Tools{

    public function insert($tabla, $campos)    {

        $key = "";
         $value = "";
        foreach ($campos as $k => $v) {
            $key .= $k . ',';
            $value .= ' "' . $v . '",';
        }
        $key = substr($key, 0, strlen($key) - 1);
        $value = substr($value, 0, strlen($value) - 1);
        $query = "INSERT INTO $tabla ($key) VALUES($value)";
        $result = mysql_query($query) or die(mysql_error());

        if ($result) {
            $id =  mysql_insert_id();
            return $id;

        }
    }

    public function update($tabla,$campos,$filter){
        $cad = "";
        foreach($campos as $k=>$v){
            $cad.= $k.'=\''.$v.'\',';
        }
        $cad = substr($cad, 0,strlen($cad)-1);
        $query = "UPDATE $tabla SET $cad WHERE $filter";  
        //die($query);   
        $result = mysql_query($query);
        if($result){
            return true;
        }  
 
    }
    
    function valInsrito($dni, $email, $id_campania){
       
        try
             {
                
          $query = "select id_inscrito from inscritos where  (email ='".$email."' or dni ='".$dni."') and campania_id=".$id_campania;     
          $result = mysql_query($query);

          $existe = 0;
          if (mysql_num_rows($result) > 0)
            $existe = 1;
      
          return $existe;
        
        }
        catch(Exception $e){ return 0;} 
    }

    function getDepartamento()
    {
        $query = "select id_ubigeo, nombre FROM ubigeos where id_provincia = '0' and id_distrito = '0'";
        $result = mysql_query($query) or die(mysql_error());
        $distrito = "";

        while ($row = mysql_fetch_array($result)) {
            $idDistrito = $row['id_ubigeo'];
            $nombreDistrito = utf8_encode($row['nombre']);
            $distrito .= "<option value='" . $idDistrito . "'>" . $nombreDistrito . "</option>";
        }

        return $distrito;
    }


    function valEmail($email, $id_campania){
       
         try
         {
            
            $query = "select id_inscrito from inscritos where email ='".$email."' and campania_id=".$id_campania;         
            $result = mysql_query($query);
            $row = mysql_fetch_array($result);
    
            if (mysql_num_rows($result) > 0) 
            {
                return $row['id_inscrito'];
            } 
            else
            {
               
              return 0;              
                        
            }
        
       }catch(Exception $e){ return 0;} 
    }

    function getNombres($id)
    {
        
        if(!empty($id) && $id > 0)  
        {
            $query = "select nombre, ap_paterno from inscritos where  id_inscrito = ".$id;         
            $result = mysql_query($query);
            $row = mysql_fetch_array($result);
            return $row['nombre'];
        }
        else
        {
            return "";
        }
       
    }

    function getPuntaje($id)
    {
        if(!empty($id) && $id > 0)  
        {
            $query = "select puntaje from inscritos where  id_inscrito = ".$id;         
            $result = mysql_query($query);
            $row = mysql_fetch_array($result);
            return $row['puntaje'];
        }
        else
        {
            return 0;
        }
    }


    function getPregunta($fecha, $pregunta)
    {
        $file = json_decode(file_get_contents('json/desafio_1.json'));

        $lista = '';      
        $button = '';  
       
        if(is_array($file))
        {
            // shuffle($file);      
            foreach ($file as $key => $value)
            {      
                                     
                if($value->fecha == $fecha)
                {
                    
                    $preguntas = $value->preguntas;
                       // var_dump($preguntas);
                    foreach ($preguntas as $p => $valor) {
                        $opciones = array();
                        
                       if($valor->pregunta_id == $pregunta)   
                        {

                            $opciones = array($valor->respuestas[0]->opcion_1, $valor->respuestas[0]->opcion_2, $valor->respuestas[0]->opcion_3);
                            shuffle($opciones);

                            $lista = '
                                <p class="title-trivia title-trivia1"><span>'.$pregunta.'. </span>'.$valor->texto.'</p>
                                            
                                <div class="opciones gen">

                                    <div class="op op1" data-toggle="id_frase" data-title="1" data-texto="'.encode_this($opciones[0]).'">
                                        <p>'.$opciones[0].'</p>
                                    </div>

                                    <div class="op op2" data-toggle="id_frase" data-title="2" data-texto="'.encode_this($opciones[1]).'">
                                        <p>'.$opciones[1].'</p>
                                    </div>


                                    <div class="op op3" data-toggle="id_frase" data-title="3" data-texto="'.encode_this($opciones[2]).'">
                                        <p>'.$opciones[2].'</p>
                                    </div>
                                    <input type="hidden" id="id_frase" name="id_frase" value="">
                                    <input type="hidden" id="txt_c" name="txt_c" value="'.encode_this($valor->respuestas[0]->opcion_1).'">
                                    <input type="hidden" id="txt_r" name="txt_r" value="">
                                    <input type="hidden" id="txt_p" name="txt_p" value="'.encode_this($valor->texto).'">
                                    <input type="hidden" id="subcategoria_id" name="subcategoria_id" value="1">

                                    <a href="javascript:void(0)" onclick="valPaso1(\''.$fecha.'\','.$pregunta.', 1);" class="omitir">omitir</a>
                                    <a href="javascript:void(0)" onclick="valPaso1(\''.$fecha.'\','.$pregunta.', 0);" class="next-gen button-gen-letsgo">
                                        <button class="">
                                            <div class="box-shadow-inset"></div>
                                            <p>siguiente</p>
                                        </button>
                                    </a>

                                </div>';    
                        } 
                    
                    }
                    
               }                                  

                
            }
        }
        
        return $lista;
    } 

    function getVideo($fecha, $pregunta)
    {
       
        $file = json_decode(file_get_contents('json/desafio_2.json'));

        $lista = '';      
        $button = '';  
       
        if(is_array($file))
        {
            // shuffle($file);      
            foreach ($file as $key => $value)
            {    
                  
                if($value->fecha == $fecha)
                {
                    
                    $preguntas = $value->preguntas;
                    //var_dump($preguntas);
                    foreach ($preguntas as $p => $valor) {
                        $opciones = array();
                        
                       if($valor->pregunta_id == $pregunta)   
                        {

                            $opciones = array($valor->respuestas[0]->opcion_1, $valor->respuestas[0]->opcion_2, $valor->respuestas[0]->opcion_3);
                            shuffle($opciones);

                            $opcion_1 = $opciones[0];
                            $opcion_2 = $opciones[1];
                            $opcion_3 = $opciones[2];

                            list($artista_1, $cancion_1) = explode("-", $opciones[0]);
                            list($artista_2, $cancion_2) = explode("-", $opciones[1]);
                            list($artista_3, $cancion_3) = explode("-", $opciones[2]);

                            $lista = '
                                <div class="video-gachi">
                                        
                                        <div style="position:relative; padding-bottom:56.25%; overflow:hidden;"><iframe src="https://content.jwplatform.com/players/'.$valor->video.'-nZpn7KM6.html" width="100%" height="100%" frameborder="0" scrolling="auto" allowfullscreen style="position:absolute;"></iframe></div>
                                    </div>

                                
                                    <div class="opciones gen">
                                        <div class="op op1 op-trivia2" data-toggle="id_frase" data-title="1" data-texto="'.encode_this($opcion_1).'">
                                            <p>'.$artista_1.'</p>
                                            <span class="artist">'.$cancion_1.'</span>
                                        </div>

                                        <div class="op op2 op-trivia2" data-toggle="id_frase" data-title="2" data-texto="'.encode_this($opcion_2).'">
                                            <p>'.$artista_2.'</p>
                                            <span class="artist">'.$cancion_2.'</span>
                                        </div>


                                        <div class="op op3 op-trivia2" data-toggle="id_frase" data-title="3" data-texto="'.encode_this($opcion_3).'">
                                            <p>'.$artista_3.'</p>
                                            <span class="artist">'.$cancion_3.'</span>
                                        </div>
                                        
                                        <input type="hidden" id="id_frase" name="id_frase" value="">
                                        <input type="hidden" id="txt_c" name="txt_c" value="'.encode_this($valor->respuestas[0]->opcion_1).'">
                                        <input type="hidden" id="txt_r" name="txt_r" value="">
                                        <input type="hidden" id="txt_p" name="txt_p" value="'.encode_this($valor->video).'">
                                        <input type="hidden" id="subcategoria_id" name="subcategoria_id" value="2">
                            

                                        <a href="javascript:void(0)" onclick="valPaso2(\''.$fecha.'\','.$pregunta.', 1);" class="omitir">omitir</a>

                                        <a href="javascript:void(0);" onclick="valPaso2(\''.$fecha.'\','.$pregunta.', 0);" class="next-gen button-gen-letsgo">
                                            <button class="">
                                                <div class="box-shadow-inset"></div>
                                                <p>siguiente</p>
                                            </button>
                                        </a>

                                    </div>';    
                        } 
                    
                    }
                    
               }                                  

                
            }
        }
        
        return $lista;
    }  

    function getInteraccion($id_campania, $id_inscrito, $fecha, $etapa)
    {
        if(!empty($id_inscrito))
        {

            $query = "select id_interaccion, cantidad from interacciones where campania_id=".$id_campania." AND inscrito_id = ".$id_inscrito." and fecha = '".$fecha."' and tipo = ".$etapa;         
            $result = mysql_query($query);
       
            $row = mysql_fetch_array($result);
    
            if (mysql_num_rows($result) >= 0) {
                $_SESSION['idint_gachi'] = $row['id_interaccion'];
                $_SESSION['cantidad_gachi'] = $row['cantidad'];
            } else {
               
                    return 0;
            }
        }
    }

     function getInteraccionEtapa3($id_campania, $id_inscrito, $fecha, $etapa)
    {
        if(!empty($id_inscrito))
        {

            $query = "select id_interaccion, cantidad from interacciones where campania_id=".$id_campania." AND inscrito_id = ".$id_inscrito." and tipo = ".$etapa;         
            $result = mysql_query($query);
       
            $row = mysql_fetch_array($result);
    
            if (mysql_num_rows($result) >= 0) {
                $_SESSION['idint_gachi'] = $row['id_interaccion'];
                $_SESSION['cantidad_gachi'] = $row['cantidad'];
            } else {
               
                    return 0;
            }
        }
    }


    function acumularPuntaje($id_inscrito, $puntaje)
    {
        $query_ins = "update inscritos SET puntaje = puntaje + ".$puntaje."  WHERE id_inscrito = ".$id_inscrito;
        mysql_query($query_ins);
        return $query_ins;
    }

    function aumentarPuntaje($id_nominado)
    {
        $query_mul = "update multimedias SET puntaje = puntaje + 1  WHERE id_multimedia = ".$id_nominado;
        mysql_query($query_mul);
    }
    

    function registrarVoto($id_inscrito, $campania, $id_nominado, $id_categoria, $fecreg, $horreg, $subcategoria_id)
    {
        $votacion['inscrito_id'] = $id_inscrito;
        $votacion['campania_id'] = mysql_real_escape_string($campania);
        $votacion['multimedia_id'] = $id_nominado;
        $votacion['categoria_id'] = $id_categoria;
        $votacion['subcategoria_id'] = $subcategoria_id;
        $votacion['fecha'] = $fecreg;
        $votacion['hora'] = $horreg;
        $id_votacion = $this->insert("votaciones_desafio_2018", $votacion);

        return $id_votacion;

    }

    function registrarRespuesta($id_inscrito, $campania, $id_nominado, $fecreg, $horreg, $pregunta_id, $tipo, $acierto, $pregunta, $respuesta)
    {
        $respuesta_inscrito['inscrito_id'] = $id_inscrito;
        $respuesta_inscrito['campania_id'] = $campania;
        $respuesta_inscrito['multimedia_id'] = $id_nominado;
        $respuesta_inscrito['categoria_id'] = $pregunta_id;
        $respuesta_inscrito['tipo'] = $tipo;
        $respuesta_inscrito['acierto'] = $acierto;
        $respuesta_inscrito['fecha'] = $fecreg;
        $respuesta_inscrito['hora'] = $horreg;
        $respuesta_inscrito['pregunta'] = utf8_decode($pregunta);
        $respuesta_inscrito['respuesta'] = utf8_decode($respuesta);
        $id_respuesta = $this->insert("resultados_inscritos", $respuesta_inscrito);

        return $id_respuesta;

    }

    function registrarMultimedia($id_inscrito, $campania, $nombre_usuario, $tipo, $fecreg, $nombre, $ruta)
    {
        $multimedia['inscrito_id'] = $id_inscrito;
        $multimedia['campania_id'] = $campania;
        $multimedia['numero'] = 3;
        $multimedia['dato3'] = $nombre_usuario;
        $multimedia['dato1'] = $tipo;
        $multimedia['fecha'] = $fecreg;
        $multimedia['dato2'] = $nombre;
        $multimedia['texto'] = $ruta;
        $multimedia['dato4'] = 50;
        $multimedia['estado'] = 0;
        $id_multimedia = $this->insert("adicionales", $multimedia);

        return $id_multimedia;

    }


    function aumentarInteraccion($id_interaccion, $cantidad)
    {
        $interaccion['cantidad'] = $cantidad;
        $this->update("interacciones" , $interaccion, "id_interaccion=".$id_interaccion);
    }

    function registrarInteraccion($cantidad, $campania, $id_inscrito,  $fecreg, $horreg, $tipo)
    {
        $interaccion['cantidad'] = $cantidad;
        $interaccion['campania_id'] = $campania;
        $interaccion['inscrito_id'] = $id_inscrito;
        $interaccion['fecha'] = $fecreg;
        $interaccion['hora'] = $horreg;
         $interaccion['tipo'] = $tipo;
        $id_interaccion =  $this->insert("interacciones", $interaccion);

        return $id_interaccion;
    }

    function getResultado($fecha)
    {
        $file = json_decode(file_get_contents('json/desafio_1.json'));

        $lista = array();      
        $x = 1;  
        
        if(is_array($file))
        {
            // shuffle($file);      
            foreach ($file as $key => $value)
            {      
                                         
                if($value->fecha == $fecha)
                {

                    $preguntas = $value->preguntas;
                       // var_dump($preguntas);
                    foreach ($preguntas as $p => $valor) {                        
                       $lista[] = array('texto' => $valor->texto, 'respuesta' => $valor->respuestas[0]->opcion_1);   
                    }                    
               }                                  

                
            }
        }
        
        return $lista;
    } 

    function getResultadosUsuario($jugadas_resultado, $subcategoria_id)
    {
        $lista = '';
        $puntaje_acumulado = 0;

        foreach ($jugadas_resultado as $jugada => $value) {
            $respuesta_usuario = decode_this($value['txt_r']);
            $respuesta_correcta = decode_this($value['txt_c']);
            $puntaje_acumulado += $value['ptje']; 
            $pregunta = decode_this($value['txt_p']); 

            if($subcategoria_id == 1)
            {
                $lista .= '<div class="question-gen">
                        <p>'.$value['id_pregunta'].'. '.$pregunta.'</p>
                    </div>';
            }
            else
            {
                $lista .= '';
            }
         

            if($value['ptje'] == 1):
                $lista .='<div class="answer">
                            <div class="top center">
                                <p class="icon icon-good"><i class="far fa-thumbs-up"></i></p>
                                <p class="answer-txt">'.$respuesta_usuario.'</p>
                            </div>
                        </div>';

            elseif(!empty($respuesta_usuario)):
                $lista .= '<div class="answer">
                            <div class="top center">
                                <p class="icon icon-bad"><i class="far fa-thumbs-down"></i></p>
                                <p class="answer-txt">'.$respuesta_usuario.'</p>
                            </div>
                           <p class="bottom">Respuesta correcta: <span class="correct">'.$respuesta_correcta.'</span></p>
                            </div>';
            
            else:
                $lista .= '<div class="answer"><p class="bottom">Respuesta correcta: <span class="correct">'.$respuesta_correcta.'</span></p></div>';
            
            endif;

                    
        }
        $lista .='<div class="class-score">
                    <p>Has acumulado <span class="score">'.$puntaje_acumulado.'</span> puntos<br>con la trivia de hoy.</p>
                </div>';
        return $lista;
    }

    function valGAleria()
    {
        $file = json_decode(file_get_contents('json/galeria.json'));

        $lista = '';    

        if(is_array($file))
        {
            // shuffle($file);      
            foreach ($file as $key => $value)
            {    
                if($value->estado == 1)
                {
                    $lista .= '<div class="wrapp-draw col-xs-12 col-sm-4">

                                    <div class="height-photo">
                                        <img src="'.$value->imagen.'" class="img-responsive center-block" alt="Dibujos">
                                    </div>

                                    <div class="data">
                                        <p><span class="name">'.$value->nombres.'</span>, <span class="age">'.$value->edad.'</span></p>
                                    </div>
                                </div>';
                }
                
            }

        }

        return $lista;
        
    }

    function calculaEdad( $fecha ) {
        list($Y,$m,$d) = explode("-",$fecha);
        return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
    }

    function valGAleriaHome()
    {
        $file = json_decode(file_get_contents('json/galeria.json'));

        $lista = '';    

        if(is_array($file))
        {
            // shuffle($file);      
            foreach ($file as $key => $value)
            {    
                if($value->estado == 1)
                {
                    $lista .= '<div class="wrapp-draw col-xs-12 col-sm-3">
                                        <img src="'.$value->imagen.'" class="img-responsive center-block" alt="Dibujos">
                                    </div>';
                }
                
            }

        }

        return $lista;
        
    }

}



?>