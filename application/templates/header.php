<script type="text/javascript">

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-39226895-1', 'auto');
ga('send', 'pageview');
</script> 

	<header id="header">

		<div class="container">
			
			<div class="user-header float-left">

			<?php if(empty($_SESSION['idins_gachi'])) : ?>
				<!-- MOSTRAR CUANDO NO ESTÁ LOGUEADO -->
				<div class="out">
					<a  data-toggle="modal" data-target="#inicio-sesion" class="init">
						<button class="icon-gen-1 icon-login">
							<i class="fas fa-user"></i>
						</button>

						<span  class="registry">iniciar sesión</span>
					</a>
				</div> <!-- fin out -->
				
				<?php else: ?>
				<!-- MOSTRAR CUANDO ESTÁ LOGUEADO -->
				<div class="in">
					<div class="float-left margin-add">
						<p class="person">hola <span class="name"><?php echo $nom_inscrito; ?></span></p>

						<?php if(!empty($ptjein_gachi)) :?>
							<div class="score-wrapp">

								<div class="center">
									<span>
										<i class="fas fa-star"></i>
									</span>

									<p class="score">
										<span class="score-data"><?php echo $ptjein_gachi; ?></span> puntos
									</p>

									<span>
										<i class="fas fa-star"></i>
									</span>
								</div>
							</div> <!-- fin score-wrapp -->
					<?php endif; ?>
					</div>


					<div class="float-left">
						<button class="icon-gen-1 icon-logout" onclick="logout();">
							<i class="fas fa-sign-out-alt"></i>
						</button>
					</div>

				</div> <!-- fin in -->	
				<?php endif; ?>				
			</div> <!-- fin user-header -->


			<div class="right-header float-right">
				
				<!--  -->
				<?php include("social-header.php"); ?>


				<button class="icon-gen-1 icon-tyc" data-toggle="modal" data-target="#TyC">
					<i class="far fa-file-alt"></i>
				</button>
				
			</div> <!-- fin right-header -->
			
		</div> <!-- fin container -->
		
	</header> <!-- fin header -->
		