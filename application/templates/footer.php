	<div class="footer-add">
		<p class="date"><a data-toggle="modal" data-target="#TyC">Ver términos y condiciones</a> | sorteo: 18 de abril</p>


		<footer class="full-footer container">	
			<div id="footer" class="container-fluid">

				<div class="content-box">
					<div class="row">
						<div class="logo-crp col-md-4 col-sm-12">
							<a href="http://www.crpweb.pe/" target="_blank"><img src="assets/images/logo-crp.png" title="Logo CRP" class="img-responsive"></a>
						</div> <!-- fin logo-crp -->

						<div class="radios-menu col-md-8">
							<ul>
								<li class="list-menu col-md-4"><a href="https://ritmoromantica.pe/" target="_blank">Ritmo Romántica</a></li>
								<li class="list-menu col-md-4"><a href="https://oasis.pe/" target="_blank">Oasis</a></li>
								<li class="list-menu col-md-4"><a href="https://planeta.pe/" target="_blank">Planeta</a></li>
								<li class="list-menu col-md-4"><a href="https://lainolvidable.pe/" target="_blank">La Inolvidable</a></li>
								<li class="list-menu col-md-4"><a href="https://radiomar.pe/" target="_blank">Radiomar</a></li>
								<li class="list-menu col-md-4"><a href="http://www.radioinca.com.pe/" target="_blank">Inca</a></li>
								<li class="list-menu col-md-4"><a href="https://moda.com.pe/" target="_blank">Moda</a></li>
								<li class="list-menu col-md-4"><a href="https://radionuevaq.pe/" target="_blank">Nueva Q</a></li>
								<li class="list-menu col-md-4"><a href="http://radiomagica.pe/" target="_blank">Mágica</a></li>
							</ul>
						</div> <!-- fin radios-menu -->
					</div>


					<div class="row">
						<div class="legal-footer">
							<div class="line">
								<div class="legal-line col-md-4">
									<p>Lincenciado por APDAYC<span><img src="assets/images/icon-apdayc.jpg"></span></p>
									
								</div>


								<div class="legal-line col-md-4">
									<p>Portal auditado por ComScore<span><img src="assets/images/icon-comscore.jpg"></span></p>
								</div>



								<div class="legal-line col-md-4">
									<p>Portal miembro del IAB<span><img src="assets/images/icon-iab.jpg"></span></p>
								</div>
							</div> <!-- fin line -->




							<div class="line">
								<div class="legal-line col-md-6">
									<p><a href="http://www.crp.pe/libro-reclamaciones/" target="_blank">Libro de reclamaciones</a></p>
								</div>


								<div class="legal-line col-md-6">
									<p><a href="http://www.crp.pe/dejanos-musica/index.php?r=crp-6" target="_blank">Déjanos tu música</a></p>
								</div>
							</div> <!-- fin line -->
						




							<div class="line">
								<div class="legal-line col-md-4">
									<p><a href="http://www.crp.pe/codigoetica.php" target="_blank">Código de Etica de SNRTV suscritos por nuestra Emisora</a></p>
								</div>


								<div class="legal-line col-md-4">
									<p><a href="http://www.crp.pe/terminosycondiciones.php" target="_blank">Términos y Condiciones</a></p>
								</div>


								<div class="legal-line col-md-4">
									<p><a href="http://www.crp.pe/quejasycomunicaciones.php" target="_blank">Reglamento de Soluciones de Quejas</a></p>
								</div>
							</div> <!-- fin line -->

							

							<div class="line">
								<div class="legal-line col-md-12">
									<p>Radio Planeta - Perú | Radios de Lima | Radios de Perú</p>
								</div>
							</div> <!-- fin line -->

						</div> <!-- fin legal-footer -->
					</div>
				</div> <!-- fin content-box -->
			</div>
		</footer>
	</div>
	





	<!-- Modal -->
	<div class="modal fade modalTyC" id="TyC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <button type="button" class="close-tyc" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>


          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Términos y condiciones</h4>
          </div>


          <div class="modal-body">
            <?php include('application/modals/tyc.php');?>
          </div> <!-- fin modal-body -->
          
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          
          </div>
        </div>
      </div>
    </div>
