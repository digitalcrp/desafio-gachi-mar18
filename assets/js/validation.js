/* useful functions */

function trim(str)
{
    while (str.charAt(0) == ' ')
        str = str.substring(1);
    while (str.charAt(str.length - 1) == ' ')
        str = str.substring(0, str.length - 1);
    return str;
}

function isEmpty(pString) {

    if (trim(pString) == "") {
        return (true);
    }
    else {
        return (false);
    }
}

function isValidNumber(e) {
    ok = "1234567890";
    for (i = 0; i < e.length; i++) {
        if (ok.indexOf(e.charAt(i)) < 0) {
            return (false);
        }
        else
            return (true);
    }
}


function isValidName(e) {
    ok = /^[á-úa-zA-ZÑ\s.\-]+$/;
    if (!e.match(ok))
        return (false);
    else
        return (true);
}

function isEmail(e) {
    ok = "1234567890qwertyuiop[]asdfghjklzxcvbnm.@-_QWERTYUIOPASDFGHJKLZXCVBNM";
    for (i = 0; i < e.length; i++) {
        if (ok.indexOf(e.charAt(i)) < 0) {
            return (false);
        }
    }
    re = /(@.*@)|(\.\.)|(^\.)|(^@)|(@$)|(\.$)|(@\.)/;
    re_two = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (!e.match(re) && e.match(re_two)) {
        return (-1);
    }
}



function caracterSpecial(obj) {

    if (obj.match(/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/))
        return (true);

    else
        return (false);

}

function onlyLetter(obj) {
    if (obj.match(/([a-zA-Z])/))
        return (true);
    else
        return (false);

}

function isValue(obj) {
    if (!obj.match(/([1-9])/))
        return (true);
    else
        return (false);

}

function valFecNac(){
    var anio = $("#anio");
    $(".error").remove();

    if (anio.val() == 0) {
        anio.focus().after("<span class='error'>Debe elegir una opci&oacute;n</span>");
        return false;
    }
}

//Se utiliza para que el campo de texto solo acepte numeros
function Event_SoloNumeros(evt){
 if(window.event){//asignamos el valor de la tecla a keynum
  keynum = evt.keyCode; //IE
 }
 else{
  keynum = evt.which; //FF
 } 
 //comprobamos si se encuentra en el rango numérico y que teclas no recibirá.
 if((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6 ){
  return true;
 }
 else{
  return false;
 }
}

function contarCaracteres(val){
     var len = val.length;
     return len;
}

function valRegistro() {

    var nombre = $("#nombres");
    var apellidos = $("#apellidos");
    var dni = $("#dni");
    var email = $("#email");
    var telefono = $("#telefono");
    var departamento = $("#departamento");
    var genero = $("#sexo");
    var anio = $("#anio");
    var mes = $("#mes");
    var dia = $("#dia");
    var tyc = $("#tyc");
   

    var error_mensaje = $("#error_mensaje");
    var error_nombres = $("#error_nombres");
    var error_apellidos = $("#error_apellidos");
    var error_dni = $("#error_dni");
    var error_email = $("#error_email");
    var error_telefono = $("#error_telefono");
    var error_departamento = $("#error_departamento");
    var error_nacimiento = $("#error_nacimiento");
    var error_genero = $("#error_sexo");
    var error_tyc = $("#error_tyc");


    $(".error").remove();

    if (isEmpty(nombre.val())) {
        error_nombres.after("<span class='error'>Es un campo obligatorio</span>");
        nombre.focus();
        return false;
    } else if (caracterSpecial(nombre.val())) {
        error_nombres.after("<span class='error'>No usar caracteres especiales</span>");
        nombre.focus();
        return false;
    } else if (!onlyLetter(nombre.val())) {
        error_nombres.after("<span class='error'>S&oacute;lo pueden ser letras</span>");
        nombre.focus();
        return false;
    }


    if (isEmpty(apellidos.val())) {
        error_apellidos.after("<span class='error'>Es un campo obligatorio</span>");
        apellidos.focus();
        return false;
    } else if (caracterSpecial(apellidos.val())) {
        error_apellidos.after("<span class='error'>No usar caracteres especiales</span>");
        apellidos.focus();
        return false;
    } else if (!onlyLetter(apellidos.val())) {
        error_apellidos.after("<span class='error'>S&oacute;lo pueden ser letras</span>");
        apellidos.focus();
        return false;
    }

    if (isEmpty(email.val())) {
        error_email.after("<span class='error'>Es un campo obligatorio</span>");
        email.focus();
        return false;
    } else if (!isEmail(email.val())) {
        error_email.after("<span class='error'>El email no es correcto</span>");
        email.focus();
        return false;
    }

    if (isEmpty(dni.val())) {
        error_dni.after("<span class='error'>Es un campo obligatorio</span>");
        dni.focus();
        return false;
    } else if (!isValidNumber(dni.val())) {
        error_dni.after("<span class='error'>S&oacute;lo pueden ser n&uacute;meros</span>");
        dni.focus();
        return false;
    }

    if (departamento.val() == 0) {
        error_departamento.after("<span class='error'>Debe elegir una opci&oacute;n</span>");
        departamento.focus();
        return false;
    } else if (!isValidNumber(departamento.val())) {
        error_departamento.after("<span class='error'>Debe elegir una opci&oacute;n</span>");
        departamento.focus();
        return false;
    }

    if (genero.val() == 0) {
        error_genero.after("<span class='error'>Debe elegir una opci&oacute;n</span>");
        genero.focus();
        return false;
    } 

    if (isEmpty(telefono.val())) {
        error_telefono.after("<span class='error'>Es un campo obligatorio</span>");
        telefono.focus();
        return false;
    } else if (!isValidNumber(telefono.val())) {
        error_telefono.after("<span class='error'>S&oacute;lo pueden ser n&uacute;meros</span>");
        telefono.focus();
        return false;
    }

    if (dia.val() == 0) {
        error_nacimiento.focus().after("<span class='error'>Debe elegir una opci&oacute;n</span>");
        return false;
    }

    if (mes.val() == 0) {
        error_nacimiento.focus().after("<span class='error'>Debe elegir una opci&oacute;n</span>");
        return false;
    }

    if (anio.val() == 0) {
        error_nacimiento.focus().after("<span class='error'>Debe elegir una opci&oacute;n</span>");
        return false;
    }

    if(!tyc.is(':checked')){
       error_tyc.after("<span class='error errortyc'>Debe aceptar los t&eacute;rminos y condiciones</span>");
       return false;
     }
 


}

function valLogin()
{
    var email = $("#email");    
    $(".error").remove();

    if (isEmpty(email.val())) {
        email.focus().after("<span class='error'>Es un campo obligatorio</span>");
        return false;
    } else if (!isEmail(email.val())) {
        email.focus().after("<span class='error'>El email no es correcto</span>");
        return false;
    }
}

function valLoginModal()
{
    var email = $("#email_login");    
    $(".error").remove();

    if (isEmpty(email.val())) {
        email.focus().after("<span class='error'>Es un campo obligatorio</span>");
        return false;
    } else if (!isEmail(email.val())) {
        email.focus().after("<span class='error'>El email no es correcto</span>");
        return false;
    }
}


function logout(){
  var $w = jQuery.noConflict();
   $w.ajax({
    type: "POST",
    url: "action.php",
    data: "action=logout",
    success: function(datos){
      window.location = "index.php";
    }
  });

}

function valPaso1(fecha, pregunta, omitir)
{

    var frase = $("#id_frase");
    $(".error").remove();

    // console.log("pregunta: "+pregunta);

    var id_frase = $("#id_frase").val();
    var txt_c = $("#txt_c").val();
    var txt_r = $("#txt_r").val();
    var txt_p = $("#txt_p").val();
    var subcategoria_id = $("#subcategoria_id").val();

    // console.log(id_frase);
    console.log("resultado: " + txt_r);

    if(omitir == 1)
    {
        $("#id_frase").val(0);
        $("#txt_r").val('');
        id_frase = 0;
    }

    // alert(id_frase);
    if(id_frase !== "")
    {
        if(txt_r === txt_c)
        {
            ptje = 1;
        }
        else
        {
            ptje = 0;
        }

        if(pregunta == 5)
        {
            action = "votar";
        }
        else
        {
            action = "pregunta";
        }

        $.ajax({
        type: "POST",
        url: "action.php",
        data: { action: action, id_frase: id_frase, fecha: fecha, id_pregunta: pregunta, ptje : ptje, txt_c: txt_c, txt_r: txt_r, txt_p:txt_p, subcategoria_id:subcategoria_id},
        success: function(datos){
            if(pregunta < 5)
            {
                $("#main-ajax").html(datos);
            }
            else
            {
                // console.log(datos);
                //alert(datos);
                window.location = "trivia1-resultados.php";
            }
            
        }
      });     
    }
    else
    {
        $('#error').modal('show');
    }

    
}


function valPaso2(fecha, pregunta, omitir)
{

    var frase = $("#id_frase");
    $(".error").remove();

    // console.log("pregunta: "+pregunta);

    var id_frase = $("#id_frase").val();
    var txt_c = $("#txt_c").val();
    var txt_r = $("#txt_r").val();
    var txt_p = $("#txt_p").val();
    var subcategoria_id = $("#subcategoria_id").val();

    // console.log(id_frase);
    console.log("resultado: " + txt_r);

    if(omitir == 1)
    {
        $("#id_frase").val(0);
        $("#txt_r").val('');
        id_frase = 0;
    }

    // alert(id_frase);
    if(id_frase !== "")
    {
        if(txt_r === txt_c)
        {
            ptje = 1;
        }
        else
        {
            ptje = 0;
        }

        if(pregunta == 7)
        {
            action = "votar";
        }
        else
        {
            action = "pregunta";
        }

        $.ajax({
        type: "POST",
        url: "action.php",
        data: { action: action, id_frase: id_frase, fecha: fecha, id_pregunta: pregunta, ptje : ptje, txt_c: txt_c, txt_r: txt_r, txt_p:txt_p, subcategoria_id:subcategoria_id},
        success: function(datos){
            if(pregunta < 7)
            {
                $("#main-ajax").html(datos);
            }
            else
            {
                // console.log(datos);
                //alert(datos);
                window.location = "trivia2-resultados.php";
            }
            
        }
      });     
    }
    else
    {
        $('#error').modal('show');
    }

    
}

function valPaso3()
{
     var files = $("#files_name").val();

     if(isEmpty(files))
     {
        $('#error').modal('show');
        return false;
     }
     else
     {
        return true;
     }
}