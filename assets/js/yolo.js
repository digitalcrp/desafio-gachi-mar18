var screenWidth = $(window).width();
var screenHeight = $(window).height(); 

initJs();

function valScreen(screenWidth)
{
    if (screenWidth > 768) {
       initJs();
       
    }
    else
    {

        $("body").attr("style", "")
    }
}

$(window).resize(function(e) {
    resizeScreen();
});

function resizeScreen() {
    screenHeight = $(window).height();
    screenWidth = $(window).width();
    valScreen(screenWidth);
}

function initJs()
{
    var scrollController = new ScrollMagic({
        refreshInterval: 200
    });

    var introTween = new TimelineMax().add([



        /*logo gachi*/
        TweenMax.fromTo(".logo-gachi-animado", 10, {
            css: {
                y: 0,
                opacity: 1
            }
        }, {
            css: {
                y: -100,
                opacity: 0
            },
            ease: Power1.easeout
        }),



        /**/
        TweenMax.fromTo(".claim-principal", 15, {
            css:{
                y: 0, opacity:1
            }
        }, {
            css:{
                y:0, 
                opacity:0,
                scaleX: 1.5, 
                scaleY: 1.5
            }
        }),


        TweenMax.fromTo(".txt-animado1", 7, {
            css: {
                y: 0,
                opacity: 1
            }
        }, {
            css: {
                y: -100,
                opacity: 0,
            },
            ease: Strong.easeInOut
        }),


        TweenMax.fromTo(".txt-animado2", 6, {
            css: {
                y: 0,
                opacity: 1
            }
        }, {
            css: {
                y: -100,
                opacity: 0,
            },
            ease: Strong.easeInOut
        }),
        /**/
     
        



        /*texto de premio*/
        TweenMax.fromTo(".txt-animado3", 10, {
            css: {
                x: 0,
                opacity: 1
            }
        }, {
            css: {
                x: -400,
                opacity: 0,
            },
            ease: Strong.easeInOut
        }),



        /*premio*/
        TweenMax.fromTo(".prize-animado", 10, {
            css: {
                x: 0,
                opacity: 1
            }
        }, {
            css: {
                x: 400,
                opacity: 0,
            },
            ease: Strong.easeInOut
        }),




        TweenMax.fromTo(".txt-animado4", 10, {
            css: {
                y: 0,
                opacity: 1
            }
        }, {
            css: {
                y: -100,
                opacity: 0,
            },
            ease: Strong.easeInOut
        }),



        TweenMax.fromTo(".play1-animado", 8, {
            css:{
                y: 0, opacity:1
            }
        }, {
            css:{
                y:100, 
                opacity:0
            },
            ease: Strong.easeInOut
        }),

    ])


    var introScene = new ScrollScene({
        duration: 1800
    }).setTween(introTween).addTo(scrollController);




/////////

// var redToYellowScene = new ScrollScene({
//         triggerElement: ".challenge1",
//         duration: 600
//     }).setTween(redToYellow).addTo(scrollController);

    var airTween = new TimelineMax().add([
        TweenMax.fromTo(".challenge1 .logo-gachi-animado1", 1, {
            css: {
               
                opacity: 0,
                scaleX: 1.5, 
                scaleY: 1.5
                    
            }
        }, {
            css: {
                
                opacity: 1,
                scaleX: 1, 
                scaleY: 1

            },
            ease: Power1.easeout
        }),



        TweenMax.fromTo(".challenge1 .gachi-animada", 1, {
            css: {
                y: 300,
                opacity: 0
                    
            }
        }, {
            css: {
                y: 0,
                opacity: 1

            },
            ease: Power1.easeout
        }),



        TweenMax.fromTo(".challenge1 .ts-animado", 1, {
            css: {
                y: -50,
                opacity: 0
                    
            }
        }, {
            css: {
                y: 0,
                opacity: 1

            },
            ease: Power1.easeout
        }),


        TweenMax.fromTo(".challenge1 .cch-animado", 1, {
            css: {
                
                opacity: 0,
                scaleX: 1.5, 
                scaleY: 1.5
            }
        }, {
            css: {
            
                opacity: 1,
                scaleX: 1, 
                scaleY: 1
                
            },
            ease: Power1.easeout
        }),


        TweenMax.fromTo(".challenge1 .info-animado", 2, {
            css: {
                y: 60,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                opacity: 1
            },
            ease: Power1.easeout
        }),





        TweenMax.fromTo(".challenge1 .bgletsgo-animado", 1, {
            css: {
                y: 60,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                opacity: 1
            },
            ease: Power1.easeout
        }),


         TweenMax.fromTo(".challenge1 .prohibition-animado", 2, {
            css: {
                y: 60,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                opacity: 1
            },
            ease: Power1.easeout
        }),



        
        TweenMax.fromTo(".challenge1 .triangulo-animado", 1, {
            css: {
                y: 200,
                rotation: 0,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                rotation: 360,
                opacity: 1
            },
            ease: Power1.easeout
        })
       
    ]);
    var airScene = new ScrollScene({
        triggerElement: ".challenge1",
        duration: ($(".challenge1").height())
    }).setTween(airTween).addTo(scrollController);








    /////////

// var redToYellowScene = new ScrollScene({
//         triggerElement: ".challenge1",
//         duration: 600
//     }).setTween(redToYellow).addTo(scrollController);

    var airTween = new TimelineMax().add([
        TweenMax.fromTo(".challenge2 .logo-gachi-animado1", 1, {
            css: {
               
                opacity: 0,
                scaleX: 1.5, 
                scaleY: 1.5
                    
            }
        }, {
            css: {
                
                opacity: 1,
                scaleX: 1, 
                scaleY: 1

            },
            ease: Power1.easeout
        }),



        TweenMax.fromTo(".challenge2 .gachi-animada", 1, {
            css: {
                y: 300,
                opacity: 0
                    
            }
        }, {
            css: {
                y: 0,
                opacity: 1

            },
            ease: Power1.easeout
        }),



        TweenMax.fromTo(".challenge2 .ts-animado", 1, {
            css: {
                y: -50,
                opacity: 0
                    
            }
        }, {
            css: {
                y: 0,
                opacity: 1

            },
            ease: Power1.easeout
        }),


        TweenMax.fromTo(".challenge2 .cch-animado", 1, {
            css: {
                
                opacity: 0,
                scaleX: 1.5, 
                scaleY: 1.5
            }
        }, {
            css: {
            
                opacity: 1,
                scaleX: 1, 
                scaleY: 1
                
            },
            ease: Power1.easeout
        }),


        TweenMax.fromTo(".challenge2 .info-animado", 2, {
            css: {
                y: 60,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                opacity: 1
            },
            ease: Power1.easeout
        }),





        TweenMax.fromTo(".challenge2 .bgletsgo-animado", 1, {
            css: {
                y: 60,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                opacity: 1
            },
            ease: Power1.easeout
        }),


         TweenMax.fromTo(".challenge2 .prohibition-animado", 2, {
            css: {
                y: 60,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                opacity: 1
            },
            ease: Power1.easeout
        }),



        
        TweenMax.fromTo(".challenge2 .circulo-animado", 1, {
            css: {
                y: -200,
                rotation: 0,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                rotation: 360,
                opacity: 1
            },
            ease: Power1.easeout
        })
       
    ]);
    var airScene = new ScrollScene({
        triggerElement: ".challenge2",
        duration: ($(".challenge2").height())-200
    }).setTween(airTween).addTo(scrollController);







    /////////

// var redToYellowScene = new ScrollScene({
//         triggerElement: ".challenge1",
//         duration: 600
//     }).setTween(redToYellow).addTo(scrollController);

    var airTween = new TimelineMax().add([
        TweenMax.fromTo(".challenge3 .logo-gachi-animado1", 1, {
            css: {
               
                opacity: 0,
                scaleX: 1.5, 
                scaleY: 1.5
                    
            }
        }, {
            css: {
                
                opacity: 1,
                scaleX: 1, 
                scaleY: 1

            },
            ease: Power1.easeout
        }),



        TweenMax.fromTo(".challenge3 .gachi-animada", 1, {
            css: {
                y: 300,
                opacity: 0
                    
            }
        }, {
            css: {
                y: 0,
                opacity: 1

            },
            ease: Power1.easeout
        }),



        TweenMax.fromTo(".challenge3 .ts-animado", 1, {
            css: {
                y: -50,
                opacity: 0
                    
            }
        }, {
            css: {
                y: 0,
                opacity: 1

            },
            ease: Power1.easeout
        }),


        TweenMax.fromTo(".challenge3 .cch-animado", 1, {
            css: {
                
                opacity: 0,
                scaleX: 1.5, 
                scaleY: 1.5
            }
        }, {
            css: {
            
                opacity: 1,
                scaleX: 1, 
                scaleY: 1
                
            },
            ease: Power1.easeout
        }),


        TweenMax.fromTo(".challenge3 .info-animado", 2, {
            css: {
                y: 60,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                opacity: 1
            },
            ease: Power1.easeout
        }),





        TweenMax.fromTo(".challenge3 .bgletsgo-animado", 1, {
            css: {
                y: 60,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                opacity: 1
            },
            ease: Power1.easeout
        }),


         TweenMax.fromTo(".challenge3 .prohibition-animado", 2, {
            css: {
                y: 60,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                opacity: 1
            },
            ease: Power1.easeout
        }),



        
        TweenMax.fromTo(".challenge3 .cuadrado-animado", 1, {
            css: {
                y: -200,
                rotation: 0,
                opacity: 0
            }
        }, {
            css: {
                y: 0,
                rotation: 360,
                opacity: 1
            },
            ease: Power1.easeout
        })
       
    ]);
    var airScene = new ScrollScene({
        triggerElement: ".challenge3",
        duration: ($(".challenge3").height())-200
    }).setTween(airTween).addTo(scrollController);

}