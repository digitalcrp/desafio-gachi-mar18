<?php
require_once("conn.php");
require_once("encode.php");

class Tools{

    public function insert($tabla, $campos)    {

        $key = "";
         $value = "";
        foreach ($campos as $k => $v) {
            $key .= $k . ',';
            $value .= ' "' . $v . '",';
        }
        $key = substr($key, 0, strlen($key) - 1);
        $value = substr($value, 0, strlen($value) - 1);
        $query = "INSERT INTO $tabla ($key) VALUES($value)";
        $result = mysql_query($query) or die(mysql_error());

        if ($result) {
            $id =  mysql_insert_id();
            return $id;

        }
    }

    public function update($tabla,$campos,$filter){
        $cad = "";
        foreach($campos as $k=>$v){
            $cad.= $k.'=\''.$v.'\',';
        }
        $cad = substr($cad, 0,strlen($cad)-1);
        $query = "UPDATE $tabla SET $cad WHERE $filter";  
        //die($query);   
        $result = mysql_query($query);
        if($result){
            return true;
        }  
 
    }
  
  function valInsrito($dni, $email, $id_campania){
       
     try
         {
            
      $query = "select id_inscrito from inscritos where  (email ='".$email."' or dni ='".$dni."') and campania_id=".$id_campania;     
      $result = mysql_query($query);

      $existe = 0;
      if (mysql_num_rows($result) > 0)
        $existe = 1;
  
      return $existe;
    
     }catch(Exception $e){ return 0;} 
  }

    function getDepartamento()
    {
        $query = "select id_ubigeo, nombre FROM ubigeos where id_provincia = '0' and id_distrito = '0'";
        $result = mysql_query($query) or die(mysql_error());
        $distrito = "";

        while ($row = mysql_fetch_array($result)) {
            $idDistrito = $row['id_ubigeo'];
            $nombreDistrito = utf8_encode($row['nombre']);
            $distrito .= "<option value='" . $idDistrito . "'>" . $nombreDistrito . "</option>";
        }

        return $distrito;
    }


    function valEmail($email, $id_campania){
       
         try
         {
            
            $query = "select id_inscrito from inscritos where email ='".$email."' and campania_id=".$id_campania;         
            $result = mysql_query($query);
            $row = mysql_fetch_array($result);
    
            if (mysql_num_rows($result) > 0) 
            {
                return $row['id_inscrito'];
            } 
            else
            {
               
              return 0;              
                        
            }
        
       }catch(Exception $e){ return 0;} 
    }

    function getNombres($id)
    {
        
        if(!empty($id) && $id > 0)  
        {
            $query = "select nombre, ap_paterno from inscritos where  id_inscrito = ".$id;         
            $result = mysql_query($query);
            $row = mysql_fetch_array($result);
            return $row['nombre'];
        }
        else
        {
            return "";
        }
       
    }


    function getPregunta($fecha, $pregunta)
    {
        $file = json_decode(file_get_contents('json/desafio_1.json'));

        $lista = '';      
        $button = '';  
       
        if(is_array($file))
        {
            // shuffle($file);      
            foreach ($file as $key => $value)
            {      
                                     
                if($value->fecha == $fecha)
                {
                    
                    $preguntas = $value->preguntas;
                       // var_dump($preguntas);
                    foreach ($preguntas as $p => $valor) {
                        $opciones = array();
                        
                       if($valor->pregunta_id == $pregunta)   
                        {

                            $opciones = array($valor->respuestas[0]->opcion_1, $valor->respuestas[0]->opcion_2, $valor->respuestas[0]->opcion_3);
                            shuffle($opciones);

                            $lista = '
                                <p class="title-trivia title-trivia1"><span>'.$pregunta.'. </span>'.$valor->texto.'</p>
                                            
                                <div class="opciones gen">

                                    <div class="op op1" data-toggle="id_frase" data-title="1" data-texto="'.encode_this($opciones[0]).'">
                                        <p>'.$opciones[0].'</p>
                                    </div>

                                    <div class="op op2" data-toggle="id_frase" data-title="2" data-texto="'.encode_this($opciones[1]).'">
                                        <p>'.$opciones[1].'</p>
                                    </div>


                                    <div class="op op3" data-toggle="id_frase" data-title="3" data-texto="'.encode_this($opciones[2]).'">
                                        <p>'.$opciones[2].'</p>
                                    </div>
                                    <input type="hidden" id="id_frase" name="id_frase" value="">
                                    <input type="hidden" id="txt_c" name="txt_c" value="'.encode_this($valor->respuestas[0]->opcion_1).'">
                                    <input type="hidden" id="txt_r" name="txt_r" value="">
                                    <input type="hidden" id="txt_p" name="txt_p" value="'.encode_this($valor->texto).'">

                                    <a href="javascript:void(0)" onclick="valPaso1(\''.$fecha.'\','.$pregunta.', 1);" class="omitir">omitir</a>
                                    <a href="javascript:void(0)" onclick="valPaso1(\''.$fecha.'\','.$pregunta.', 0);" class="next-gen button-gen-letsgo">
                                        <button class="">
                                            <div class="box-shadow-inset"></div>
                                            <p>siguiente</p>
                                        </button>
                                    </a>

                                </div>';    
                        } 
                    
                    }
                    
               }                                  

                
            }
        }
        
        return $lista;
    }  

    function getInteraccion($id_campania, $id_inscrito, $fecha)
    {
        if(!empty($id_inscrito))
        {

            $query = "select id_interaccion, cantidad from interacciones where campania_id=".$id_campania." AND inscrito_id = ".$id_inscrito." and fecha = '".$fecha."'";         
            $result = mysql_query($query);
            $row = mysql_fetch_array($result);
    
            if (mysql_num_rows($result) >= 0) {
                $_SESSION['idint_gachi'] = $row['id_interaccion'];
                $_SESSION['cantidad_gachi'] = $row['cantidad'];
            } else {
               
                    return 0;
            }
        }
    }


    function acumularPuntaje($id_inscrito, $puntaje)
    {
        $query_ins = "update inscritos SET puntaje = puntaje + ".$puntaje."  WHERE id_inscrito = ".$id_inscrito;
        mysql_query($query_ins);
        return $query_ins;
    }

    function aumentarPuntaje($id_nominado)
    {
        $query_mul = "update multimedias SET puntaje = puntaje + 1  WHERE id_multimedia = ".$id_nominado;
        mysql_query($query_mul);
    }
    

    function registrarVoto($id_inscrito, $campania, $id_nominado, $id_categoria, $fecreg, $horreg)
    {
        $votacion['inscrito_id'] = $id_inscrito;
        $votacion['campania_id'] = mysql_real_escape_string($campania);
        $votacion['multimedia_id'] = $id_nominado;
        $votacion['categoria_id'] = $id_categoria;
        $votacion['fecha'] = $fecreg;
        $votacion['hora'] = $horreg;
        $id_votacion = $this->insert("votaciones", $votacion);

        return $id_votacion;

    }

    function aumentarInteraccion($id_interaccion, $cantidad)
    {
        $interaccion['cantidad'] = $cantidad;
        $this->update("interacciones" , $interaccion, "id_interaccion=".$id_interaccion);
    }

    function registrarInteraccion($cantidad, $campania, $id_inscrito,  $fecreg, $horreg)
    {
        $interaccion['cantidad'] = $cantidad;
        $interaccion['campania_id'] = $campania;
        $interaccion['inscrito_id'] = $id_inscrito;
        $interaccion['fecha'] = $fecreg;
        $interaccion['hora'] = $horreg;
        $id_interaccion =  $this->insert("interacciones", $interaccion);

        return $id_interaccion;
    }

    function getResultado($fecha)
    {
        $file = json_decode(file_get_contents('json/desafio_1.json'));

        $lista = array();      
        $x = 1;  
        
        if(is_array($file))
        {
            // shuffle($file);      
            foreach ($file as $key => $value)
            {      
                                         
                if($value->fecha == $fecha)
                {

                    $preguntas = $value->preguntas;
                       // var_dump($preguntas);
                    foreach ($preguntas as $p => $valor) {                        
                       $lista[] = array('texto' => $valor->texto, 'respuesta' => $valor->respuestas[0]->opcion_1);   
                    }                    
               }                                  

                
            }
        }
        
        return $lista;
    } 

    function getResultadosUsuario($jugadas_resultado)
    {
        $lista = '';
        $puntaje_acumulado = 0;

        foreach ($jugadas_resultado as $jugada => $value) {
            $respuesta_usuario = decode_this($value['txt_r']);
            $respuesta_correcta = decode_this($value['txt_c']);
            $puntaje_acumulado += $value['ptje']; 
            $pregunta = decode_this($value['txt_p']); 

            $lista .= '<div class="question-gen">
                        <p>'.$value['id_pregunta'].'. '.$pregunta.'</p>
                    </div>';

                    if($value['ptje'] == 1):
                        $lista .='<div class="answer">
                                    <div class="top center">
                                        <p class="icon icon-good"><i class="far fa-thumbs-up"></i></p>
                                        <p class="answer-txt">'.$respuesta_usuario.'</p>
                                    </div>
                                </div>';

                    elseif(!empty($respuesta_usuario)):
                        $lista .= '<div class="answer">
                                    <div class="top center">
                                        <p class="icon icon-bad"><i class="far fa-thumbs-down"></i></p>
                                        <p class="answer-txt">'.$respuesta_usuario.'</p>
                                    </div>
                                   <p class="bottom">Respuesta correcta: <span class="correct">'.$respuesta_correcta.'</span></p>
                                    </div>';
                    
                    else:
                        $lista .= '<div class="answer"><p class="bottom">Respuesta correcta: <span class="correct">'.$respuesta_correcta.'</span></p></div>';
                    
                    endif;

                    
        }
        $lista .='<div class="class-score">
                    <p>Has acumulado <span class="score">'.$puntaje_acumulado.'</span> puntos<br>con la trivia de hoy.</p>
                </div>';
    return $lista;
    }

    

}

?>