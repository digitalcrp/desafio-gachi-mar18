<script type="text/javascript">

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-39226895-1', 'auto');
ga('send', 'pageview');
</script> 
	<header id="header">

		<div class="container">
			
			


			<div class="right-header float-right">
				
				<a class="icon-gen-1 icon-whatsapp" href="whatsapp://send?text=¡Conoce a Gachi y gana grandes premios!" onclick="ga('send', 'event', 'Desafío Oh my Gachi', 'Whatsapp');">
					<i class="fab fa-whatsapp"></i>
				</a>


				<a class="icon-gen-1 icon-facebook" href="https://www.facebook.com/sharer/sharer.php?u=http://concursos.crp.pe/planeta/campanias/minisites/desafio-gachi-mar18/" target="_blank" onclick="ga('send', 'event', 'Desafío Oh my Gachi', 'compartir');">
					<i class="fab fa-facebook-f"></i>
				</a>


				<button class="icon-gen-1 icon-tyc" data-toggle="modal" data-target="#TyC">
					<i class="far fa-file-alt"></i>
				</button>
				
			</div> <!-- fin right-header -->
			
		</div> <!-- fin container -->
		
	</header> <!-- fin header -->
		