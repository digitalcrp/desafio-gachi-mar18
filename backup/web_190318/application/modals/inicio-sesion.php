<button type="button" class="btn-circle-gen btn-close-modal" data-dismiss="modal" aria-label="Close">
	<i class="fa fa-times" aria-hidden="true"></i>
</button>

<p class="txt-title-modal">para jugar debes iniciar sesión</p>

<form action="action.php" method="POST" class="clearfix" onSubmit="return valLoginModal();"> 
	<input type="hidden" id="action" name="action" value="login"> 
	<input type="email" name="email" id="email_login" class="inp-gen" placeholder="Ingresa tu email" required>	

	<div class="animated infinite pulse">
		<button class="button-gen-send btn-send" type="submit">
			<div class="box-shadow-inset"></div>
			<p>enviar</p>
		</button>
	</div>

</form>

<p class="txt-register"><span>¿primera vez?</span><a href="registro.php"> regístrate aquí</a></p>