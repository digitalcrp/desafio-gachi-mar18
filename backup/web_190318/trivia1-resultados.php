<?php 
require_once("application/config/conn.php");
require_once("application/config/tools.php");

$tools = new Tools();
session_start();

$id_inscrito = NULL;
$nom_inscrito = NULL;

if(!empty($_SESSION['idins_gachi']))
{
  	$id_inscrito = $_SESSION['idins_gachi'];
  	$nom_inscrito = $_SESSION['nomins_gachi'];

  	if($_SESSION['estado'] == 0)
  	{
  		if(!empty($_SESSION['resultados_inscrito']))
	  	{
	  		$jugadas_resultado =  unserialize($_SESSION['resultados_inscrito']);
			unset($_SESSION['pregunta_inscrito']);
			unset($_SESSION['jugadas_inscrito']);
	  	}
	  	else
	  	{
	  		header("Location: index.php");
	  	}
  	}
  	else
  	{
  		header("Location: index.php");
  	}
}
else
{
	header("Location: index.php");
}

// var_dump($jugadas_resultado);
// var_dump($resultados);
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Desafío Oh My Gachi! de Radio Planeta</title>

	<link rel="stylesheet" type="text/css" href="assets/css/trivias.min.css?V2">

	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/global.min.js"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


	<link rel="apple-touch-icon" sizes="57x57" href="https://planeta.pe/assets/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="https://planeta.pe/assets/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="https://planeta.pe/assets/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="https://planeta.pe/assets/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="https://planeta.pe/assets/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="https://planeta.pe/assets/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="https://planeta.pe/assets/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="https://planeta.pe/assets/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="https://planeta.pe/assets/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="https://planeta.pe/assets/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="https://planeta.pe/assets/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="assets/css/validation.css" rel="stylesheet">
	<script type="text/javascript" src="assets/js/validation.js"></script>
	
</head>




<body class="back-trivia1">

	
	

	<?php include('application/templates/header.php');?>
		


	<section id="core-wrapp">	

		<article class="content-trivias article-gen">
			
		
				<div class="container relative fixed">	

					<div class="row">


						<?php include ('application/templates/topinn.php');?>

						


						<div class="col-xs-12 bottom">
							<div class="col-sm-3">
								<img src="assets/images/gachi/gachi1.png?V1" class="gachi1 img-responsive center-block gachi-animada gachi1-trivia" alt="Gachi">
							</div>


							<div class="col-xs-12 col-sm-6">
								<div class="center">
									<h2 class="txt-gen txt-title-tipo1 txt-dotted">desafío 1</h2>
								</div>

								<p class="res">resultados</p>

								<div class="wrapp-trivia wrapp-trivia1">
									<div class="box-shadow-inset"></div>

									<div class="resultados">

										<?php echo $tools->getResultadosUsuario($jugadas_resultado); ?>

										
						
										<div class="dato">Vuelve mañana y responde <br>las nuevas preguntas de la trivia.</div>
					
										
									</div>

									<div class="mini-figures mini-figures1">
										<img src="assets/images/mini-figures/figure1.png" class="img-responsive center-block" alt="Figura 1">
									</div>

									<div class="mini-figures mini-figures2">
										<img src="assets/images/mini-figures/figure3.png" class="img-responsive center-block" alt="Figura 2">
									</div>

									<div class="mini-figures mini-figures3">
										<img src="assets/images/mini-figures/figure2.png" class="img-responsive center-block" alt="Figura 3">
									</div>

									<div class="mini-figures mini-figures4">
										<img src="assets/images/mini-figures/figure4.png" class="img-responsive center-block" alt="Figura 4">
									</div>


									

								</div> <!-- fin wrapp-trivia -->
						
								<div class="animated infinite pulse">
										
									<a href="https://planeta.pe/radioenvivo" target="_blank">
										<button class="button-gen-letsgo center letsradio">
											<div class="box-shadow-inset"></div>
											<p>Escucha radio planeta aquí</p>
										</button>
									</a>
										
								</div>




							</div>


						</div> <!-- fin bottom -->

					</div> <!-- fin row -->

				</div><!-- fin container -->
					


		</article> <!-- fin content-index -->

	</section><!-- fin core-wrapp -->
	



	<?php include ('application/templates/footer.php');?>


	<script type="text/javascript">
		$(document).ready(function() {
			$(".op").click(function(){
			
				$(".op").removeClass("select");
				$(this).addClass("select");

				var sel = $(this).data('title');
				var tog = $(this).data('toggle');
				$('#'+tog).prop('value', sel);
			});
		});
	</script>


	

</body>
</html>