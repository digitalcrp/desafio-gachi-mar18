<?php 
require_once("application/config/conn.php");
require_once("application/config/tools.php");

$tools = new Tools();
session_start();

$id_inscrito = NULL;
$nom_inscrito = NULL;

if(!empty($_SESSION['idins_gachi']))
{
  $id_inscrito = $_SESSION['idins_gachi'];
  $nom_inscrito = $_SESSION['nomins_gachi'];

  if($_SESSION['estado'] != 1)
  {
	header("Location: index.php?msg=limite");
  }

}
else
{
	header("Location: index.php");
}



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Desafío Oh My Gachi! de Radio Planeta</title>

	<link rel="stylesheet" type="text/css" href="assets/css/trivias.min.css?V=3">

	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/global.min.js"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


	<link rel="apple-touch-icon" sizes="57x57" href="https://planeta.pe/assets/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="https://planeta.pe/assets/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="https://planeta.pe/assets/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="https://planeta.pe/assets/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="https://planeta.pe/assets/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="https://planeta.pe/assets/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="https://planeta.pe/assets/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="https://planeta.pe/assets/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="https://planeta.pe/assets/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="https://planeta.pe/assets/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="https://planeta.pe/assets/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="assets/css/validation.css" rel="stylesheet">
	<script type="text/javascript" src="assets/js/validation.js?v=4"></script>
	
</head>




<body class="back-trivia1">

	
	

	<?php include('application/templates/header.php');?>
		


	<section id="core-wrapp">	

		<article class="content-trivias article-gen">
			
		
				<div class="container relative fixed">	

					<div class="row">

						<?php include ('application/templates/topinn.php');?>


						<div class="col-xs-12 bottom">
							<div class="col-sm-3">
								<img src="assets/images/gachi/gachi1.png?V1" class="gachi1 img-responsive center-block gachi-animada gachi1-trivia" alt="Gachi">
							</div>


							<div class="col-xs-12 col-sm-6">
								<div class="center">
									<h2 class="txt-gen txt-title-tipo1 txt-dotted">desafío 1</h2>
								</div>

								<div id="main-ajax">
									<?php include("main.php"); ?>
								</div> <!-- fin wrapp-trivia -->
							</div>





							
						</div> <!-- fin bottom -->

					</div> <!-- fin row -->

				</div><!-- fin container -->
					


		</article> <!-- fin content-index -->

	</section><!-- fin core-wrapp -->
	



	<?php include ('application/templates/footer.php');?>

	<div class="wrapper-modal-gen">
	    <div class="modalexiste modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

		    <div class="modal-dialog" role="document">
		    	<div class="content-box">
		        	<?php include ('application/modals/error.php');?>
		        </div>
		    </div>
	    </div>
    </div> <!-- fin wrapper-modal-gen -->
	


	

</body>
</html>