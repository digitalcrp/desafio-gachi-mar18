<?php 
require_once("application/config/conn.php");
require_once("application/config/tools.php");

$tools = new Tools();
session_start();

$id_inscrito = NULL;
$nom_inscrito = NULL;

if(!empty($_SESSION['idins_gachi']))
{
  $id_inscrito = $_SESSION['idins_gachi'];
  $nom_inscrito = $_SESSION['nomins_gachi'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Desafío Oh My Gachi! de Radio Planeta</title>

	<meta name="description" content="¿Quieres conocer a Gachi? Participa cumpliendo los desafíos y acumulando puntos. ¡A más puntos, más oportunidades de ganar! Además, podrás ganar un audífono Skullcandy o una GoPro y CD de tus artistas favoritos. Sorteo: 18 de abril. Términos y condiciones en planeta.pe." />
	<meta name="keytwords" content="radio planeta, radio Gachi, Graciela Rivero, Gachi Rivero, Oh My Gachi, música, actual, inglés, gana, Go Pro, gopro, skullcandy, audífono skullcandy, meet&greet, m&g, gratis, concurso, participa, perú, lima, electro, pop, hip-hop." />

	<meta property="og:title" content="¡Conoce a Gachi y gana grandes premios!"/>
	<meta property="og:description" content="Participa cumpliendo los desafíos y acumulando puntos. ¡A más puntos, más oportunidades de ganar! Además, podrás ganar un audífono Skullcandy o una GoPro y CD de tus artistas favoritos. Sorteo: 18 de abril. Términos y condiciones en planeta.pe."/>
	<meta property="og:url" content="http://concursos.crp.pe/planeta/campanias/minisites/desafio-gachi-mar18/"/>
	<meta property="og:image" content="http://concursos.crp.pe/planeta/campanias/minisites/desafio-gachi-mar18/assets/images/facebook-560.png"/>


	<link rel="stylesheet" type="text/css" href="assets/css/home.min.css?V3">

	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/global.min.js"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


	<link rel="apple-touch-icon" sizes="57x57" href="https://planeta.pe/assets/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="https://planeta.pe/assets/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="https://planeta.pe/assets/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="https://planeta.pe/assets/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="https://planeta.pe/assets/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="https://planeta.pe/assets/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="https://planeta.pe/assets/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="https://planeta.pe/assets/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="https://planeta.pe/assets/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="https://planeta.pe/assets/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="https://planeta.pe/assets/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="assets/css/validation.css" rel="stylesheet">
	<script type="text/javascript" src="assets/js/validation.js?v=1"></script>
	<script type="text/javascript" src="assets/js/jquery.smooth-scroll.js"></script>
	
	


	<link href="assets/css/fixed-positioning.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />

</head>




<body>
<div class="overflow">

	<!-- <div id="bg1" data-0="background-position:0px 0px;" data-end="background-position:-500px -10000px;"></div>
	<div id="bg2" data-0="background-position:0px 0px;" data-end="background-position:-500px -8000px;"></div>
	<div id="bg3" data-0="background-position:0px 0px;" data-end="background-position:-500px -6000px;"></div>
	<div id="bg4" data-0="background-position:0px 0px;" data-end="background-position:-500px -10000px;"></div> -->
	

	<?php include('application/templates/header.php');?>
		


	<section id="core-wrapp">	

		<article class="content-index">
			
			<div class="container-fluid">	
				

				<div class="container relative fixed">	
					

					
					<nav class="nav-challange">
						<p class="main-txt txt-gen txt-dotted">desafío</p>
						<div class="line"></div>

						<a href="#play1" class="play1 center">
							<div class="anchor-wrapp anchor-wrapp1">
								<div class="bright"></div>
								<div class="wrapp-number center">
									<p class="type1">1</p>
								</div>
							</div> <!-- fin anchor-wrapp -->
						</a>

						<a href="#play2" class="play2 center">
							<div class="anchor-wrapp anchor-wrapp2">
								<div class="bright"></div>
								<div class="wrapp-number center">
									<p class="type2">2</p>
								</div>
							</div> <!-- fin anchor-wrapp -->
						</a>


						<a href="#play3" class="play2 center">
							<div class="anchor-wrapp anchor-wrapp3">
								<div class="bright"></div>
								<div class="wrapp-number center">
									<p class="type3">3</p>
								</div>
							</div> <!-- fin anchor-wrapp -->
						</a>
				
					</nav> <!-- fin nav-challange --> 




					<div class="row">

						<section class="principal">

							<div class="col-xs-10 col-sm-12 top">
								<!-- <p style="height: 1200px;">p</p> -->

								<div class="col-xs-2">
									<a href="https://planeta.pe/" target="_blank">
										<img src="assets/images/logo-planeta.png" class="logo-planeta img-responsive center-block" alt="Logo Planeta">
									</a>

									<p class="wiii">presenta:</p>
								</div>

								<div class="col-xs-8 logo-gachi-animado">
									<img src="assets/images/logo-gachi.png" class="logo-gachi img-responsive center-block animated bounceInDown" alt="Log Desafío Oh My Gachi!">
								</div>

							</div> <!-- fin top -->


							<div class="col-xs-10 col-sm-12 bottom">
								<div class="col-sm-8 center">
									
									<div class="animated bounceIn">
										<div class="claim-principal ">
											<div class="box-shadow-inset"></div>
											<h1 class="txt-gen txt-1 txt-center txt-animado1">¿Quieres conocer a Gachi?</h1>
											<p class="txt-gen txt-2 txt-center txt-animado2">¡Participa cumpliendo<br>los desafíos!</p>
										</div>
									</div>
									


									<div class="prize-wrapp">
										<div class="col-sm-6 txt-animado3 txt-mobile-hidden">
											<p class="txt-gen txt-3 animated bounceInLeft txt-animado3">y podrás ganar un<br><span>Meet & Greet con ella,</span><br> un audífono Skullcandy o una GoPro y CD de tus artistas favoritos.</p>
										</div>


										<div class="col-sm-6 txt-mobile-show">
											<p class="txt-gen txt-3 animated bounceIn">y podrás ganar un<br><span>Meet & Greet con ella</span><br>y muchos premios más.</p>
											<div class="arrow-mob">
												<img src="assets/images/arrow-mob.png" class="arrow img-responsive center-block" alt="Flecha">
											</div>
										</div>


										<div class="col-xs-10 col-sm-6 prize-animado wrapp-prize-center">
											<img src="assets/images/prize.png" class="prize img-responsive center-block animated bounceInRight" alt="Imágenes premios">
										</div>
									</div> <!-- fin prize-wrapp -->

									<div class="txt-animado4">
										<p class="txt-gen txt-4 animated bounceIn ">¡serán 2 ganadores!</p>
									</div>
									


									<div class="animated infinite pulse">
										<div class="play1-animado">
											<a href="#play1" class="play1" onclick="ga('send', 'event', 'Desafío Oh my Gachi', 'Participar');">
												<button class="button-gen-letsgo letsgo center animated bounceIn">
													<div class="box-shadow-inset"></div>
													<p>participar</p>
												</button>
											</a>
										</div>
									<div>

								</div>
							</div> <!-- fin bottom -->


						</section> <!-- fin principal -->

					</div> <!-- fin row -->

				</div><!-- fin container -->
					





				
				<div class="row">
					
					<div class="an">
						<div id="play1"></div>
						<div class="form1">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 600 90" style="enable-background:new 0 0 600 90;" xml:space="preserve">
							<style type="text/css">
								.st0{fill:#FFE400;}
							</style>
							<g>
								<polygon class="st0" points="600,90 0,90 0,0"/>
							</g>
							</svg>
						</div>


						<div class="form1">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 600 90" style="enable-background:new 0 0 600 90;" xml:space="preserve">
							<style type="text/css">
								.st0{fill:#FFE400;}
							</style>
							<g>
								<polygon class="st0" points="0,90 600,90 600,0"/>
							</g>
							</svg>
						</div>
					</div> <!-- fin an -->
				





					<section class="challenge challenge1">
						
						<div class="container">

							<div class="row">

								<div class="col-sm-3">
									<img src="assets/images/logo-gachi.png" class="logo-gachi img-responsive center-block logo-gachi-animado1" alt="Log Desafío Oh My Gachi!">

									<img src="assets/images/gachi/gachi1.png?V1" class="gachi1 img-responsive center-block gachi-animada" alt="Gachi">

									<div class="normal">
										<img src="assets/images/figure1.png" class="img-responsive center-block figure figure1 triangulo-animado" alt="Triángulo">
									</div>
								</div>

								<div class="col-xs-10 col-sm-6">
									<div class="title-seccion center ts-animado">
										<p class="txt-gen txt-ts txt-dotted">desafío <span class="play"><i class="fas fa-play"></i></span></p>

										<div class="circle-ts center">
											<div class="bright"></div>
											<p>1</p>
										</div>
									</div> <!-- fin title-seccion -->

									<h2 class="txt-gen claim-challenge txt-center cch-animado">¿Qué tanto conoces de Radio Planeta y Gachi?</h2>
									<p class="txt-gen info txt-center info-animado">Responde la trivia y acumula puntos. Cada pregunta respondida correctamente es un punto más.</p>
									

									<div class="animated infinite pulse bgletsgo-animado">
										
										<?php 
											$enlace = 'data-toggle="modal" data-target="#inicio-sesion"';
											if(!empty($_SESSION['idins_gachi']))
											{
												$enlace = 'href="trivia1.php"';

											}	
											?>
										<a <?php echo $enlace; ?> onclick="ga('send', 'event', 'Desafío Oh my Gachi', 'Desafío 1');">
											<button class="button-gen-letsgo letsgo center bgletsgo-animado">
												<div class="box-shadow-inset"></div>
												<p>jugar <i class="fas fa-play-circle"></i></p>
											</button>
										</a>

									</div>



									<p class="txt-gen prohibition txt-center prohibition-animado">Puedes responder 5 preguntas al día. Escucha algunas respuestas al aire en Oh My Gachi!</p>
								</div>





							</div> <!-- fin row -->

						</div> <!-- fin container -->

					</section> <!-- fin challenge1 -->

					



					<div class="an">
						<div id="play2"></div>

						<div class="form2">
							<svg version="1.1" id="Capa_1b" style="margin-top: -5px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1200 56" style="enable-background:new 0 0 1200 56;" xml:space="preserve">
								<style type="text/css">
									.st0{fill:#FFE400;}
									.st1{fill:#FF00D2;}
								</style>
								<g>
									<polygon class="st0" points="1200.1,56 0,0 1200.1,0"/>
								</g>
								<g>
									<polygon class="st1" points="0,6 1200.1,56 0,56"/>
								</g>
							</svg>
						</div>
					</div>

					<section class="challenge challenge2">
						<div class="container">							
							<div class="row">
								<div class="col-sm-3">
									<img src="assets/images/logo-gachi.png" class="logo-gachi img-responsive center-block logo-gachi-animado1" alt="Log Desafío Oh My Gachi!">

									<img src="assets/images/gachi/gachi2.png?V1" class="gachi2 img-responsive center-block gachi-animada" alt="Gachi">

									<div class="normal">
										<img src="assets/images/figure2.png" class="img-responsive center-block figure figure2 circulo-animado" alt="Círculo">
									</div>
								</div>




								<div class="col-xs-10 col-sm-6">
									<div class="title-seccion center ts-animado">
										<p class="txt-gen txt-ts txt-dotted">desafío <span class="play"><i class="fas fa-play"></i></span></p>

										<div class="circle-ts center">
											<div class="bright"></div>
											<p>2</p>
										</div>
									</div> <!-- fin title-seccion -->

									<h2 class="txt-gen claim-challenge txt-center cch-animado">¡Adivina la canción que Gachi tararea! </h2>
									<p class="txt-gen info txt-center info-animado">Cada canción adivinada <br>correctamente es un punto más.</p>
									




									<div class="animated- infinite- pulse- bgletsgo-animado relative">
										<div class="not">
											<p>este reto se activará <br>el 28 de marzo</p>
										</div>

										<a href="javascript:void(0);" onclick="ga('send', 'event', 'Desafío Oh my Gachi', 'Desafío 2');">
											<button class="button-gen-letsgo letsgo center bgletsgo-animado">
												<div class="box-shadow-inset"></div>
												<p>jugar <i class="fas fa-play-circle"></i></p>
											</button>
										</a>

									</div>



									<p class="txt-gen prohibition txt-center prohibition-animado">Puedes adivinar 2 canciones al día.</p>
								</div>


							</div>

						</div> <!-- fin container -->

					</section> <!-- fin challenge2 -->

					


					<div class="an">
						<div id="play3"></div>

						<div class="form2">
							<svg version="1.1" id="Capa_2" style="margin-top: -5px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1200 56" style="enable-background:new 0 0 1200 56;" xml:space="preserve">
							<style type="text/css">
								.st3{fill:#FF00D2;}
								.st4{fill:#00EAFF;}
							</style>
							<g>
								<polygon class="st3" points="0,56 1200.1,0 0,0 	"/>
							</g>
							<g>
								<polygon class="st4" points="1200.1,6 0,56 1200.1,56 	"/>
							</g>
							</svg>
						</div>

					</div>

					<section class="challenge challenge3">
						<div class="container">

							<div class="row">

								<div class="col-sm-3">
									<img src="assets/images/logo-gachi.png" class="logo-gachi img-responsive center-block logo-gachi-animado1" alt="Log Desafío Oh My Gachi!">

									<img src="assets/images/gachi/gachi4.png?V1" class="gachi3 img-responsive center-block gachi-animada" alt="Gachi">

									<img src="assets/images/figura3.png" class="img-responsive center-block figure figure3 cuadrado-animado" alt="Cuadrado">
									
								</div>

								<div class="col-xs-10 col-sm-6">
									<div class="title-seccion center ts-animado">
										<p class="txt-gen txt-ts txt-dotted">desafío <span class="play"><i class="fas fa-play"></i></span></p>

										<div class="circle-ts center">
											<div class="bright"></div>
											<p>3</p>
										</div>
									</div> <!-- fin title-seccion -->

									<h2 class="txt-gen claim-challenge txt-center cch-animado">¡Dibuja a Gachi!</h2>
									<p class="txt-gen info txt-center info-animado">Envíanos tu creación y Gachi podría elegirla como una de sus favoritas.</p>
									

									<div class="relative animated- infinite- pulse- bgletsgo-animado">
										<div class="not">
											<p>este reto se activará <br>el 4 de abril</p>
										</div>
										<a href="javascript:void(0);" onclick="ga('send', 'event', 'Desafío Oh my Gachi', 'Desafío 3');">
											<button class="button-gen-letsgo letsgo center bgletsgo-animado">
												<div class="box-shadow-inset"></div>
												<p>envía tu dibujo</p>
											</button>
										</a>
									</div>



									<p class="txt-gen prohibition txt-center prohibition-animado">Solo puedes subir un dibujo <br>y obtendrás 50 puntos</p>
								</div>





							</div> <!-- fin row -->

							<div class="gallery col-xs-10" style="display: none;">
								<h3 class="txt-gen txt-center">Mira los dibujos favoritos de Gachi</h3>
								<div class="college">
									<div class="wrapp-draw col-xs-12 col-sm-3">
										<img src="assets/images/dibujos/prueba1.jpg" class="img-responsive center-block" alt="Dibujos">
									</div>
									<div class="wrapp-draw col-xs-12 col-sm-3">
										<img src="assets/images/dibujos/prueba2.jpg" class="img-responsive center-block" alt="Dibujos">
									</div>
									<div class="wrapp-draw col-xs-12 col-sm-3">
										<img src="assets/images/dibujos/prueba1.jpg" class="img-responsive center-block" alt="Dibujos">
									</div>
									<div class="wrapp-draw col-xs-12 col-sm-3">
										<img src="assets/images/dibujos/prueba2.jpg" class="img-responsive center-block" alt="Dibujos">
									</div>
								</div> <!-- fin college -->



								<div class="animated infinite pulse">
									<a href="javascript:void(0);">
										<button class="button-gen-letsgo letsgo letsgo-min center">
											<div class="box-shadow-inset"></div>
											<p>ver más <i class="fas fa-plus-circle"></i></p>
										</button>
									</a>
								</div>
							</div>

						</div> <!-- fin container -->

					</section> <!-- fin challenge3 -->

				</div> <!-- fin row -->
				
				
				

			</div> <!-- fin container-fluid -->

		</article> <!-- fin content-index -->

	</section><!-- fin core-wrapp -->
		
<!-- Modal -->
    <div class="wrapper-modal-gen">
	    <div class="modalexiste modal fade" id="limite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

		    <div class="modal-dialog" role="document">
		    	<div class="content-box">
		        	<?php include ('application/modals/limite.php');?>
		        </div>
		    </div>
	    </div>
    </div> <!-- fin wrapper-modal-gen -->

	

	<!-- Modal -->
    <div class="wrapper-modal-gen">
	    <div class="modalexiste modal fade" id="inicio-sesion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

		    <div class="modal-dialog" role="document">
		    	<div class="content-box">
		        	<?php include ('application/modals/inicio-sesion.php');?>
		        </div>
		    </div>
	    </div>
    </div> <!-- fin wrapper-modal-gen -->


	<!-- Modal -->
    <div class="wrapper-modal-gen">
	    <div class="modalexiste modal fade" id="gracias-registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

		    <div class="modal-dialog" role="document">
		    	<div class="content-box">
		        	<?php include ('application/modals/gracias.php');?>
		        </div>
		    </div>
	    </div>
    </div> <!-- fin wrapper-modal-gen -->

	<?php include ('application/templates/footer.php');?>
<div id="scrollbar" data-0="top:0%;margin-top:2px;" data-end="top:100%;margin-top:-52px;"></div>

	<script type="text/javascript" src="assets/js/skrollr.min.js"></script>
	<script type="text/javascript" src="assets/js/TweenMax.js"></script>
	<script src="assets/js/jquery.scrollmagic.js"></script> 

	
	<!-- nuevo -->
	<script type="text/javascript">
		/*/*parallax*/
		// var s = skrollr.init({
		// 	edgeStrategy: 'set',
		// 	easing: {
		// 	WTF: Math.random,
		// 		inverted: function(p) {
		// 			return 1-p;
		// 		}
		// 	}
		// });


		/*smooth scroll */
		$('.play1, .play2, .play3').click(function(event) {
	        event.preventDefault();
	        var link = this;
	        $.smoothScroll({
	          scrollTarget: link.hash
	        });
	      });

		$parent = $('.fixed').eq(0);
		$elem = $parent.find('.nav-challange');

		$(window).scroll(function(){ 
		    $elem.css('top', $(window).scrollTop());
		}).trigger('scroll');
	</script>
	



<!-- <script type="text/javascript" src="assets/js/yolo.js?v1"></script> -->
</div>

<?php 
if(!empty($_GET['msg'])){	
	$msg = $_GET['msg'];
	
	switch ($msg) {
		case 'gracias':
			$modal = 'gracias-registro';
			break;

		case 'limite':
			$modal = 'limite';
			break;
		
		default:
			# code...
			break;
	}
	?>
			<script type="text/javascript">
	          $('#<?php echo $modal; ?>').modal('show');
	        </script>
	<?php 
	}
?>


</body>
</html>