<?php

require_once("application/config/tools.php");
$tools = new Tools();

$jugada_inscrito = NULL;
if(!empty($_SESSION['jugada_inscrito']))
{
	$jugada_inscrito = $_SESSION['jugada_inscrito'];
}

if(!empty($_SESSION['pregunta_inscrito']))
{
	$pregunta_inscrito = $_SESSION['pregunta_inscrito'];
}

if(empty($pregunta_inscrito))
{
	$pregunta_inscrito = 1;
}

$select_1 = '';
$select_2 = '';
$select_3 = '';
$select_4 = '';
$select_5 = '';

switch ($pregunta_inscrito ) {
	case 1:
		$select_1 = 'dot-select';
		break;

	case 2:
		$select_2 = 'dot-select';
		break;

	case 3:
		$select_3 = 'dot-select';
		break;

	case 4:
		$select_4 = 'dot-select';
		break;

	case 5:
		$select_5 = 'dot-select';
		break;
	
	default:
		# code...
		break;
}

?>


<nav class="nav-trivia">
	<a href="javascript:void(0);" class="dot <?php echo $select_1; ?>"></a>
	<a href="javascript:void(0);" class="dot <?php echo $select_2; ?>"></a>
	<a href="javascript:void(0);" class="dot <?php echo $select_3; ?>"></a>
	<a href="javascript:void(0);" class="dot <?php echo $select_4; ?>"></a>
	<a href="javascript:void(0);" class="dot <?php echo $select_5; ?>"></a>
</nav> <!-- fin nav-trivia -->

<div class="wrapp-trivia wrapp-trivia1">
	<div class="box-shadow-inset"></div>

		<?php echo $tools->getPregunta(date("d/m/Y"), $pregunta_inscrito);?>



	<div class="mini-figures mini-figures1">
		<img src="assets/images/mini-figures/figure1.png" class="img-responsive center-block" alt="Figura 1">
	</div>

	<div class="mini-figures mini-figures2">
		<img src="assets/images/mini-figures/figure3.png" class="img-responsive center-block" alt="Figura 2">
	</div>

	<div class="mini-figures mini-figures3">
		<img src="assets/images/mini-figures/figure2.png" class="img-responsive center-block" alt="Figura 3">
	</div>

	<div class="mini-figures mini-figures4">
		<img src="assets/images/mini-figures/figure4.png" class="img-responsive center-block" alt="Figura 4">
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(".op").click(function(){
		
			$(".op").removeClass("select");
			$(this).addClass("select");

			var sel = $(this).data('title');
			var tog = $(this).data('toggle');
			var texto = $(this).data('texto');
			$('#'+tog).prop('value', sel);

			console.log(texto);
			$('#txt_r').prop('value', texto);
		});
	});

ga('send', 'event', 'Desafío Oh my Gachi', 'Desafío 1', 'Pregunta <?php echo $pregunta_inscrito; ?>');
ga('send', 'pageview', 'planeta/campanias/minisites/desafio-gachi-mar18/pregunta<?php echo $pregunta_inscrito; ?>');

</script>
