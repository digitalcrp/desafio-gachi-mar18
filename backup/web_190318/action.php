<?php
setlocale(LC_ALL, 'es_ES');
require_once("application/config/conn.php");
require_once("application/config/tools.php");
require_once("application/config/emblue.php");
require_once("application/config/encode.php");
session_start();


$tool = new Tools();

$action = $_POST['action'];
$campania = 376;
$fecreg = date("Y-m-d");
$horreg = date("H:i:s");
$fecha = date("Y-m-d H:i:s");


switch ($action) {

    case "login":
        $email = $_POST['email'];

        if(!empty($email))
        {
            // Valida si el usuario (email) existe en la BD
            $id_inscrito = $tool->valEmail($email, $campania);
            $pagina = 'index.php';

            // el usuario ya existe    
            if ($id_inscrito > 0) 
            {    
                $nombre_inscrito = $tool->getNombres($id_inscrito);

                // Guardo su nombre y ID en sesiones
                $_SESSION['idins_gachi'] = $id_inscrito;
                $_SESSION['nomins_gachi'] =  $nombre_inscrito;

                // Obtengo su código de interaccion
                $tool->getInteraccion($campania, $id_inscrito, $fecreg);
                $id_interaccion = NULL;
                $cantidad = NULL;

                if(!empty($_SESSION['idint_gachi']))
                {
                    $id_interaccion = $_SESSION['idint_gachi'];
                }

                if(!empty($_SESSION['cantidad_gachi']))
                {
                    $cantidad = $_SESSION['cantidad_gachi']; 
                }       
               
                if(empty($cantidad))
                {
                    $_SESSION['estado'] = 1;
                    unset($_SESSION['jugadas_inscrito']);
                    unset($_SESSION['resultados_inscrito']);
                    unset($_SESSION['pregunta_inscrito']);
                    header('Location: trivia1.php');
                }   
                else{
                    $_SESSION['estado'] = 2;
                    header('Location: index.php');
                }                      
            }
            else
            {
           
                $param['email'] = $email;
                $param = encode_this(serialize($param));
                $_SESSION['datos_inscrito'] = $param;

                header('Location: registro.php');
            }

        }
        else
        {
            header('Location: index.php');
            
        }
    break;

    case "registro":
        //Variables
        $nombres = $_POST['nombres'];
        $apellidos = $_POST['apellidos'];   
        $telefono = $_POST['telefono'];       
        $dni = $_POST['dni'];        
        $email = $_POST['email'];
        $departamento = $_POST['departamento'];
        $genero = $_POST['sexo'];
        $dia = $_POST['dia'];
        $mes = $_POST['mes'];
        $anio = $_POST['anio'];
        $fechayhora = mktime(0, 0, 0, $mes, $dia, $anio);
        $fechayhora = date('Y-m-d h:i:s', $fechayhora);
   
        if(empty($_POST['boletin']))
        {
            $boletin = 0;
        }
        else
        {
            $boletin = 1;
        }

        //Validar Campos
        if(!empty($nombres) && !empty($apellidos) && !empty($telefono) && !empty($dni))
        {
            if(!empty($email) && !empty($dni))
            {
                $id_inscrito = $tool->valInsrito($dni, $email, $campania);

                if ($id_inscrito == 0) 
                {
                    $inscrito['nombre'] = mysql_real_escape_string(utf8_decode($nombres));
                    $inscrito['ap_paterno'] = mysql_real_escape_string(utf8_decode($apellidos));
                    $inscrito['dni'] = mysql_real_escape_string($dni);
                    $inscrito['email'] = mysql_real_escape_string($email);
                    $inscrito['telefono'] = mysql_real_escape_string($telefono);
                    $inscrito['fecha_registro'] = $fecreg;
                    $inscrito['hora_registro'] = $horreg;
                    $inscrito['ubigeo_id'] = $departamento;
                    $inscrito['genero'] = $genero;
                    $inscrito['fecha_nacimiento'] = $fechayhora;
                    $inscrito['campania_id'] = mysql_real_escape_string($campania);
                    $inscrito['acepto_boletin'] = mysql_real_escape_string($boletin);
                    $inscrito['acepto_tyc'] = 1;
                    $inscrito['estado'] = 1;
                    $inscrito['puntaje'] = 1;
                    $id_inscrito = $tool->insert("inscritos", $inscrito);

                    $_SESSION['idins_gachi'] = $id_inscrito;
                    $_SESSION['nomins_gachi'] =  $nombres;

                    $emblue = new Emblue();

                    if ($boletin == 1) 
                    {
                       // Emblue
                       
                        $user_emblue = $emblue->addContact($nombres, $email, $apellidos, $telefono, $dni, $genero, $fechayhora);

                        $datos_emblue = array(
                            'id_inscrito' => $id_inscrito,
                            'id_campania' => $campania,
                            'id_emblue' => $user_emblue->EmailId,
                            'estado' => $user_emblue->Description,
                            'respuesta' => $user_emblue->ContactoEstadoId
                        );
                        $tool->insert("inscritos_detalle", $datos_emblue);
                    }

                    $_SESSION['estado']  = 1;
                    header('Location: index.php?msg=gracias');  
                }
                else
                {                   
                    header("Location: registro.php?msg=existe");  
                }
            }
        }
        else
        {
           header("Location: index.php");
        }
    break;

    case "votar":
        $id_frase = $_POST['id_frase'];
        $fecha = $_POST['fecha'];
        $id_pregunta = $_POST['id_pregunta'];
        $ptje = $_POST['ptje'];     

        $txt_r = $_POST['txt_r'];
        $txt_c = $_POST['txt_c'];
        $txt_p = $_POST['txt_p'];

        if(empty($_SESSION['jugadas_inscrito']))
        {
            $jugadas = array();
            $resultados = array();
        }
        else
        {
            $jugadas = unserialize($_SESSION['jugadas_inscrito']);
            $resultados = unserialize($_SESSION['resultados_inscrito']);
        }

        
        $valor = array( 'id_pregunta' => $id_pregunta, 'id_frase' => $id_frase, 'ptje' => $ptje);
        array_push ( $jugadas , $valor );

        $valor2 = array( 'id_pregunta' => $id_pregunta, 'txt_c' => $txt_c, 'txt_r' => $txt_r,'ptje' => $ptje, 'txt_p' => $txt_p);
        array_push ( $resultados , $valor2 );

        // var_dump($resultados);


        $_SESSION['jugadas_inscrito'] = serialize($jugadas);
        $_SESSION['resultados_inscrito'] = serialize($resultados);
        // $_SESSION['pregunta_inscrito'] = $id_pregunta + 1;
       

        if($id_frase >= 0 && $id_frase <=5)
        {
               $id_inscrito = $_SESSION['idins_gachi'];

                //Interaccion de usuario
                if(!empty($_SESSION['idint_gachi']))
                {
                    $id_interaccion = $_SESSION['idint_gachi']; 
                }
                
                if(!empty($_SESSION['cantidad_gachi']))
                {
                    $cantidad = $_SESSION['cantidad_gachi'];
                }
                  
                if(!empty($id_interaccion))
                {
                    $_SESSION['estado'] = 2;
                    echo "limite";
                }
                else
                {
                    // Registar Interacción
                    $cantidad = 1;
                    $id_interaccion = $tool->registrarInteraccion($cantidad, $campania, $id_inscrito,  $fecreg, $horreg);

                    $_SESSION['cantidad_gachi'] = $cantidad;
                    $_SESSION['idint_gachi'] = $id_interaccion;
                    $ptje = 0;

                        foreach ($jugadas as $jugada => $value) {
                            // Registrar voto en la BD
                            $tool->registrarVoto($id_inscrito, $campania, $value['id_frase'], $value['id_pregunta'], $fecreg, $horreg);

                            //Aumentar puntaje
                            //$tool->aumentarPuntaje($value['id_frase']);

                            //Acumular puntaje
                            $ptje += $value['ptje'];
                            
                        }

                    $tool->acumularPuntaje($id_inscrito, $ptje);

                    $_SESSION['estado'] = 0;
                    echo "voto-exitoso";         
                }                
           
        }
        else
        {
           header("Location: index.php");
        }

        break;

    case "pregunta":
        $id_frase = $_POST['id_frase'];
        $fecha = $_POST['fecha'];
        $id_pregunta = $_POST['id_pregunta'];
        $ptje = $_POST['ptje'];
        $txt_r = $_POST['txt_r'];
        $txt_c = $_POST['txt_c'];
        $txt_p = $_POST['txt_p'];

        if(empty($_SESSION['jugadas_inscrito']))
        {
            $datos = array();
            $resultados = array();
            $_SESSION['resultados_inscrito'] = NULL;
            $_SESSION['jugadas_inscrito'] = NULL;
        }
        else
        {
            $datos = unserialize($_SESSION['jugadas_inscrito']);
            $resultados = unserialize($_SESSION['resultados_inscrito']);
        }

        
        $valor = array( 'id_pregunta' => $id_pregunta, 'id_frase' => $id_frase, 'ptje' => $ptje);
        array_push ( $datos , $valor );

        $valor2 = array( 'id_pregunta' => $id_pregunta, 'txt_c' => $txt_c, 'txt_r' => $txt_r,'ptje' => $ptje, 'txt_p' => $txt_p);
        array_push ( $resultados , $valor2 );


        $_SESSION['jugadas_inscrito'] = serialize($datos);
        $_SESSION['resultados_inscrito'] = serialize($resultados);
        $_SESSION['pregunta_inscrito'] = $id_pregunta + 1;
        $_SESSION['estado'] = 1;

        include("main.php"); 

    break;

    case "logout":
        session_unset();
        session_write_close();
        header("Location: index.php");
    break;

    default:
        header("Location: index.php");
    break;
}
?>
