
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>

	<meta name="description" content="" />
	<meta name="keytwords" content="" />

	<meta property="og:title" content=""/>
	<meta property="og:description" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:image" content=""/>


	<link rel="stylesheet" type="text/css" href="assets/css/trivias.min.css?V1">

	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/global.min.js"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


	<link rel="apple-touch-icon" sizes="57x57" href="https://planeta.pe/assets/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="https://planeta.pe/assets/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="https://planeta.pe/assets/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="https://planeta.pe/assets/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="https://planeta.pe/assets/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="https://planeta.pe/assets/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="https://planeta.pe/assets/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="https://planeta.pe/assets/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="https://planeta.pe/assets/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="https://planeta.pe/assets/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="https://planeta.pe/assets/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="assets/css/validation.css" rel="stylesheet">
	<script type="text/javascript" src="assets/js/validation.js"></script>
	
</head>




<body class="back-trivia2">

	
	

	<?php include('application/templates/header.php');?>
		


	<section id="core-wrapp">	

		<article class="content-trivias article-gen">
			
		
				<div class="container relative fixed">	

					<div class="row">

						<?php include ('application/templates/topinn.php');?>


						<div class="col-xs-12 bottom">
							<div class="col-sm-3">
								<img src="assets/images/gachi/gachi2.png?v1" class="gachi2 img-responsive center-block gachi-animada gachi2-trivia" alt="Gachi">
							</div>


							<div class="col-xs-11 col-sm-6">
								<div class="center">
									<h2 class="txt-gen txt-title-tipo1 txt-dotted">desafío 2</h2>
								</div>


								<nav class="nav-trivia">
									<a href="" class="dot dot-select"></a>
									<a href="" class="dot"></a>
								</nav> <!-- fin nav-trivia -->



								<div class="wrapp-trivia wrapp-trivia2">
									<div class="box-shadow-inset"></div>

									<p class="title-trivia title-trivia2">¡Adivina la canción que Gachi tararea!</p>

									<div class="video-gachi">
										<iframe width="560" height="315" src="https://www.youtube.com/embed/2we4kSptOlk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
									</div>

									<form>
										<div class="opciones gen">
					                        <div class="op op1 op-trivia2" data-toggle="frase" data-title="1">
					                            <p>Caravan</p>
					                            <span class="artist">Duke Ellington</span>
					                        </div>

					                        <div class="op op2 op-trivia2" data-toggle="frase" data-title="2">
					                            <p>Chained To The Rhythm</p>
					                            <span class="artist">Katy Perry</span>
					                        </div>


					                        <div class="op op3 op-trivia2" data-toggle="frase" data-title="3">
					                            <p>Yellow Submarine</p>
					                            <span class="artist">The Beatles</span>
					                        </div>
					                        <input type="hidden" id="frase" name="frase" value="1">


					                        <a href="trivia1-b.php" class="omitir">omitir</a>
					                    </div>
									</form>

									<div class="mini-figures mini-figures1">
										<img src="assets/images/mini-figures/figure1.png" class="img-responsive center-block" alt="Figura 1">
									</div>

									<div class="mini-figures mini-figures2">
										<img src="assets/images/mini-figures/figure3.png" class="img-responsive center-block" alt="Figura 2">
									</div>

									<div class="mini-figures mini-figures3">
										<img src="assets/images/mini-figures/figure2.png" class="img-responsive center-block" alt="Figura 3">
									</div>

									<div class="mini-figures mini-figures4">
										<img src="assets/images/mini-figures/figure4.png" class="img-responsive center-block" alt="Figura 4">
									</div>


									<a href="">
										<button class="next-gen next-gen-trivia2">
											<div class="box-shadow-inset"></div>
											<i class="fas fa-angle-right"></i>
										</button>
									</a>

								</div> <!-- fin wrapp-trivia -->
							</div>





							
						</div> <!-- fin bottom -->

					</div> <!-- fin row -->

				</div><!-- fin container -->
					


		</article> <!-- fin content-index -->

	</section><!-- fin core-wrapp -->
	



	<?php include ('application/templates/footer.php');?>


	<script type="text/javascript">
		$(document).ready(function() {
			$(".op").click(function(){
			
				$(".op").removeClass("select");
				$(this).addClass("select");

				var sel = $(this).data('title');
				var tog = $(this).data('toggle');
				$('#'+tog).prop('value', sel);
			});
		});
	</script>


	

</body>
</html>