
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>

	<meta name="description" content="" />
	<meta name="keytwords" content="" />

	<meta property="og:title" content=""/>
	<meta property="og:description" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:image" content=""/>


	<link rel="stylesheet" type="text/css" href="assets/css/trivias.min.css?V1">

	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/global.min.js"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


	<link rel="apple-touch-icon" sizes="57x57" href="https://planeta.pe/assets/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="https://planeta.pe/assets/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="https://planeta.pe/assets/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="https://planeta.pe/assets/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="https://planeta.pe/assets/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="https://planeta.pe/assets/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="https://planeta.pe/assets/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="https://planeta.pe/assets/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="https://planeta.pe/assets/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="https://planeta.pe/assets/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="https://planeta.pe/assets/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="assets/css/validation.css" rel="stylesheet">
	<script type="text/javascript" src="assets/js/validation.js"></script>
	
</head>




<body class="back-trivia3">

	
	

	<?php include('application/templates/header.php');?>
		


	<section id="core-wrapp">	

		<article class="content-trivias article-gen">
			
		
				<div class="container relative fixed">	

					<div class="row">

						<?php include ('application/templates/topinn.php');?>


						<div class="col-xs-12 bottom">


							<p class="txt-gen txt-correct-t3 txt-dotted txt-shadow shadow margin-add">Los favoritos de Gachi</p>


							<div class="college college-trivia3">
								<div class="wrapp-draw col-xs-12 col-sm-4">

									<div class="height-photo">
										<img src="assets/images/dibujos/prueba1.jpg" class="img-responsive center-block" alt="Dibujos">
									</div>

									<div class="data">
										<p><span class="name">fausto</span>, <span class="age">10 años</span></p>
									</div>
								</div>

								<div class="wrapp-draw col-xs-12 col-sm-4">

									<div class="height-photo">
										<img src="assets/images/dibujos/prueba2.jpg" class="img-responsive center-block" alt="Dibujos">
									</div>

									<div class="data">
										<p><span class="name">diana</span>, <span class="age">7 años</span></p>
									</div>
								</div>

								<div class="wrapp-draw col-xs-12 col-sm-4">

									<div class="height-photo">
										<img src="assets/images/dibujos/prueba1.jpg" class="img-responsive center-block" alt="Dibujos">
									</div>

									<div class="data">
										<p><span class="name">perséfone</span>, <span class="age">5 años</span></p>
									</div>
								</div>
							</div>



							<div class="animated infinite pulse">
										
								<a href="" target="_blank">
									<button class="button-gen-letsgo center letsradio">
										<div class="box-shadow-inset"></div>
										<p>envía tu dibujo</p>
									</button>
								</a>
									
							</div>
					
							
						</div> <!-- fin bottom -->

					</div> <!-- fin row -->

				</div><!-- fin container -->
					


		</article> <!-- fin content-index -->

	</section><!-- fin core-wrapp -->
	



	<?php include ('application/templates/footer.php');?>


	<script type="text/javascript">
		$(document).ready(function() {
			$(".op").click(function(){
			
				$(".op").removeClass("select");
				$(this).addClass("select");

				var sel = $(this).data('title');
				var tog = $(this).data('toggle');
				$('#'+tog).prop('value', sel);
			});
		});
	</script>


	

</body>
</html>