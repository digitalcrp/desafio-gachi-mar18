
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>

	<meta name="description" content="" />
	<meta name="keytwords" content="" />

	<meta property="og:title" content=""/>
	<meta property="og:description" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:image" content=""/>


	<link rel="stylesheet" type="text/css" href="assets/css/extras.min.css?V1">

	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/global.min.js"></script>


	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


	<link rel="apple-touch-icon" sizes="57x57" href="https://planeta.pe/assets/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="https://planeta.pe/assets/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="https://planeta.pe/assets/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="https://planeta.pe/assets/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="https://planeta.pe/assets/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="https://planeta.pe/assets/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="https://planeta.pe/assets/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="https://planeta.pe/assets/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="https://planeta.pe/assets/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="https://planeta.pe/assets/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="https://planeta.pe/assets/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="assets/css/validation.css" rel="stylesheet">
	<script type="text/javascript" src="assets/js/validation.js"></script>
	
	
</head>




<body class="back-extras">
	<?php include('application/templates/header-registro.php');?>
	
	<section id="core-wrapp">	

		<article class="content-extras article-gen">
		

				<div class="container relative">	
					
					<div class="row">

						<?php include ('application/templates/topinn.php');?>

						<div class="content-register-in">
							<div class="center">
								<h2 class="txt-gen txt-title-tipo1 txt-dotted">el concurso ha finalizado</h2>
							</div>

							<p class="txt-gen txt-indi">El nombre de los 2 ganadores será publicado el 18 de abril, a las 5 p.m. en planeta.pe</p>

							

							<div class="animated infinite pulse">
										
								<a href="https://planeta.pe" target="_blank">
									<button class="button-gen-letsgo center letsradio">
										<div class="box-shadow-inset"></div>
										<p>ir a planeta.pe</p>
									</button>
								</a>
									
							</div>

						

						</div> <!-- fin content-register-in -->

					</div>

				</div> <!-- fin container -->


		</article> <!-- fin content-extras -->

	</section> <!-- fin core-wrapp -->


	<?php include ('application/templates/footer.php');?>
	

	<!-- Modal -->
    <div class="wrapper-modal-gen">
	    <div class="modalexiste modal fade" id="existe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

		    <div class="modal-dialog" role="document">
		    	<div class="content-box">
		        	<?php include ('application/modals/existe.php');?>
		        </div>
		    </div>
	    </div>
    </div> <!-- fin wrapper-modal-gen -->

 
</body>
</html>