<?php 
require_once("application/config/conn.php");
require_once("application/config/tools.php");

$tools = new Tools();
session_start();

$id_inscrito = NULL;
$nom_inscrito = NULL;

if(!empty($_SESSION['idins_gachi']))
{
  $id_inscrito = $_SESSION['idins_gachi'];
  $nom_inscrito = $_SESSION['nomins_gachi'];
  $ptjein_gachi = $_SESSION['ptjein_gachi'];
  
  if($_SESSION['estado'] != 1)
  {
	header("Location: index.php?msg=limite");
  }

}
else
{
	header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>

	<meta name="description" content="" />
	<meta name="keytwords" content="" />

	<meta property="og:title" content=""/>
	<meta property="og:description" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:image" content=""/>


	<link rel="stylesheet" type="text/css" href="assets/css/trivias.min.css?V1">

	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/global.min.js"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


	<link rel="apple-touch-icon" sizes="57x57" href="https://planeta.pe/assets/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="https://planeta.pe/assets/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="https://planeta.pe/assets/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="https://planeta.pe/assets/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="https://planeta.pe/assets/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="https://planeta.pe/assets/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="https://planeta.pe/assets/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="https://planeta.pe/assets/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="https://planeta.pe/assets/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="https://planeta.pe/assets/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="https://planeta.pe/assets/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="assets/css/validation.css" rel="stylesheet">
	<script type="text/javascript" src="assets/js/validation.js?v=4"></script>
	
</head>




<body class="back-trivia3">

	
	

	<?php include('application/templates/header.php');?>
		


	<section id="core-wrapp">	

		<article class="content-trivias article-gen">
			
		
				<div class="container relative fixed">	

					<div class="row">

						<?php include ('application/templates/topinn.php');?>


						<div class="col-xs-12 col-sm-10 bottom center">
							


							<div class="col-xs-12 col-sm-8">
								<div class="center">
									<h2 class="txt-gen txt-title-tipo1 txt-dotted">desafío 3</h2>
								</div>


								<div class="up">
									<form action="action.php" method="post" onsubmit="return valPaso3();">

										<div class="">
											<input type="hidden" name="action" id="action" value="pregunta_3">
											<div id="files" class="txt-gen files col-xs-8"><div class="box-shadow-inset"></div><p>&nbsp;</p></div>
											<input type="hidden" name="files_name" id="files_name" value="">
											
											<span class="btn btn-success fileinput-button  col-xs-4">
												<div class="box-shadow-inset"></div>
										        <span>subir <i class="fas fa-arrow-up"></i></span>
										        <!-- The file input field used as target for the file upload widget -->
										        <input id="fileupload" type="file" name="files[]">
										    </span>
										</div>

									    <div class="animated infinite pulse clear padding-add">		
										
											<button class="button-gen-letsgo center letsradio">
												<div class="box-shadow-inset"></div>
												<p>Enviar</p>
											</button>										
												
										</div>
									</form>
								</div>

								<div class="kind">
									<div class="title">Ten en cuenta:</div>

									<ul class="list">
										<li class="list-item"><i class="fas fa-hand-point-right"></i><span>Tu dibujo no debe pesar más de 3 MB.</span></li>
										<li class="list-item"><i class="fas fa-hand-point-right"></i><span>Solo se admiten formatos png y jpg.</span></li>
									</ul> <!-- fin list -->
								</div>
							</div>

							<div class="col-sm-4">
								<img src="assets/images/gachi/gachi4.png?V1" class="gachi3 img-responsive center-block gachi-animada gachi3-trivia" alt="Gachi">
							</div>
							
						</div> <!-- fin bottom -->

					</div> <!-- fin row -->

				</div><!-- fin container -->
					


		</article> <!-- fin content-index -->

	</section><!-- fin core-wrapp -->
	
	<?php include ('application/templates/footer.php');?>


	<div class="wrapper-modal-gen">
	    <div class="modalexiste modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

		    <div class="modal-dialog" role="document">
		    	<div class="content-box">
		        	<?php include ('application/modals/error_dibujo.php');?>
		        </div>
		    </div>
	    </div>
    </div> <!-- fin wrapper-modal-gen -->


	<script type="text/javascript">
		$(document).ready(function() {
			$(".op").click(function(){
			
				$(".op").removeClass("select");
				$(this).addClass("select");

				var sel = $(this).data('title');
				var tog = $(this).data('toggle');
				$('#'+tog).prop('value', sel);
			});
		});
	</script>

<script src="assets/js/jquery.ui.widget.js?v=3"></script>
<script src="assets/js/jquery.iframe-transport.js?v=3"></script>
<script src="assets/js/jquery.fileupload.js?v=3"></script>
<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = 'upload.php';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        add: function (e, data) {
	        var uploadErrors = [];

	        // var acceptFileTypes = /\/(jpg|png|jpeg|wav)$/i;
	        // if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
	        //     uploadErrors.push('Tipo de archivo no aceptado');
	        // }

	        console.log(data.originalFiles[0]['size']) ;
	        if (data.originalFiles[0]['size'] > 1000000) {
	            uploadErrors.push('Tamaño de archivo demasiado grande');
	        }
	        if(uploadErrors.length > 0) {
	            alert(uploadErrors.join("\n"));
	        } else {
	            data.context = $('<p/>').text('Subiendo...').appendTo(document.body);	           
	            data.submit();
	        }

	    },
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
            	$('#files').empty();
                $('<p/>').text(file.name).appendTo('#files');
                $('<div class="box-shadow-inset"></div>').appendTo('#files');
                 $("#files_name").val(file.name);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>

</body>
</html>