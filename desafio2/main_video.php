<?php

require_once("application/config/tools.php");
$tools = new Tools();

$jugada_inscrito = NULL;
if(!empty($_SESSION['jugada_inscrito'][2]))
{
	$jugada_inscrito = $_SESSION['jugada_inscrito'][2];
}

if(!empty($_SESSION['pregunta_inscrito'][2]))
{
	$pregunta_inscrito = $_SESSION['pregunta_inscrito'][2];
}

if(empty($pregunta_inscrito) || ($pregunta_inscrito < 6 || $pregunta_inscrito >7))
{
	$pregunta_inscrito = 6;
	$_SESSION['pregunta_inscrito'][2] = NULL;
	// $_SESSION['resultados_inscrito'] = NULL;
	
}

$select_6 = '';
$select_7 = '';

switch ($pregunta_inscrito ) {
	case 6:
		$select_6 = 'dot-select';
		break;

	case 7:
		$select_7 = 'dot-select';
		break;	
}

?>

<div class="center">
	<h2 class="txt-gen txt-title-tipo1 txt-dotted">desafío 2</h2>
</div>


<nav class="nav-trivia">
	<a href="javascript:void(0);" class="dot <?php echo $select_6; ?>"></a>
	<a href="javascript:void(0);" class="dot <?php echo $select_7; ?>"></a>
</nav> <!-- fin nav-trivia -->



<div class="wrapp-trivia wrapp-trivia2">
	<div class="box-shadow-inset"></div>

	<p class="title-trivia title-trivia2">¡Adivina la canción que Gachi tararea!</p>
	
	<?php echo $tools->getVideo(date("d/m/Y"), $pregunta_inscrito);?>							

	<div class="mini-figures mini-figures1">
		<img src="assets/images/mini-figures/figure1.png" class="img-responsive center-block" alt="Figura 1">
	</div>

	<div class="mini-figures mini-figures2">
		<img src="assets/images/mini-figures/figure3.png" class="img-responsive center-block" alt="Figura 2">
	</div>

	<div class="mini-figures mini-figures3">
		<img src="assets/images/mini-figures/figure2.png" class="img-responsive center-block" alt="Figura 3">
	</div>

	<div class="mini-figures mini-figures4">
		<img src="assets/images/mini-figures/figure4.png" class="img-responsive center-block" alt="Figura 4">
	</div>


	
</div> <!-- fin wrapp-trivia -->

<script type="text/javascript">
	$(document).ready(function() {
		$(".op").click(function(){
		
			$(".op").removeClass("select");
			$(this).addClass("select");

			var sel = $(this).data('title');
			var tog = $(this).data('toggle');
			var texto = $(this).data('texto');
			$('#'+tog).prop('value', sel);

			console.log(texto);
			$('#txt_r').prop('value', texto);
		});
	});

ga('send', 'event', 'Desafío Oh my Gachi', 'Desafío 2', 'Pregunta <?php echo $pregunta_inscrito; ?>');
ga('send', 'pageview', 'planeta/campanias/minisites/desafio-gachi-mar18/pregunta<?php echo $pregunta_inscrito; ?>');

</script>
