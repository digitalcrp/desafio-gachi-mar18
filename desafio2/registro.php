<?php 
require_once("application/config/tools.php");
require_once("application/config/encode.php");
session_start();

$tools = new Tools();

$param = '';

if(!empty($_SESSION['datos_inscrito']))
{
  $param = $_SESSION['datos_inscrito'];
  $datos_inscrito = decode_this($param);
  $datos_inscrito = unserialize($datos_inscrito);
  $email = $datos_inscrito['email'];

}
else
{
  $email = '';
  $id_categoria = NULL;
  $id_nominado = NULL;
}

if(!empty($_SESSION['idins_gachi']))
{
  header("Location: index.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Desafío Oh My Gachi! de Radio Planeta</title>

	<link rel="stylesheet" type="text/css" href="assets/css/register.min.css?V2">

	<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="assets/js/global.min.js"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


	<link rel="apple-touch-icon" sizes="57x57" href="https://planeta.pe/assets/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="https://planeta.pe/assets/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="https://planeta.pe/assets/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="https://planeta.pe/assets/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="https://planeta.pe/assets/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="https://planeta.pe/assets/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="https://planeta.pe/assets/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="https://planeta.pe/assets/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="https://planeta.pe/assets/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="https://planeta.pe/assets/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="https://planeta.pe/assets/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="https://planeta.pe/assets/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link href="assets/css/validation.css" rel="stylesheet">
	<script type="text/javascript" src="assets/js/validation.js?v=4"></script>
	
	
</head>




<body class="back-registro">
	<?php include('application/templates/header-registro.php');?>

	
	
	<section id="core-wrapp">	

		<article class="content-register article-gen">
		

				<div class="container relative">	

					
					<div class="row">

						<?php include ('application/templates/topinn.php');?>

						<div class="content-register-in">
							<div class="center">
								<h2 class="txt-gen txt-title-tipo1 txt-dotted">registro</h2>
							</div>
							<p class="txt-gen txt-indi">ingresa tus datos para poder participar</p>



							<div class="wrapp-register col-xs-12">

								<form action="action.php" method="post" onSubmit="return valRegistro();" id="register">
				                <input id="action" name="action" type="hidden" value="registro"/> 

									<div class="overflow">

										<div class="item-register col-sm-4 col-xs-12">
											<label class="label-gen label-form">Nombres:</label>
											<input class="inp-gen inp-form" type="text" name="nombres" id="nombres" placeholder="Ingresa tus nombres" required>
											<span class="error-form" id="error_nombres"></span>
										</div> <!-- fin item-register -->


										<div class="item-register col-sm-4 col-xs-12">
											<label class="label-gen label-form">Apellidos:</label>
											<input class="inp-gen inp-form" type="text" name="apellidos" id="apellidos" placeholder="Ingresa tus apellidos" required>
											<span class="error-form" id="error_apellidos"></span>
										</div> <!-- fin item-register -->


										<div class="item-register col-sm-4 col-xs-12">
											<label class="label-gen label-form">D.N.I.: </label>
											<input class="inp-gen inp-form" type="text" name="dni" id="dni" placeholder="Ingresa tu D.N.I." maxlength="8" onkeypress="return Event_SoloNumeros(event);" required minlength="8">
											<span class="error-form" id="error_dni"></span>
										</div> <!-- fin item-register -->



										<div class="item-register col-sm-4 col-xs-12">
											<label class="label-gen label-form">e-mail:</label>
											<input class="inp-gen inp-form" type="email" name="email" id="email" placeholder="Ingresa tu email" value="<?php echo $email; ?>" required>
											<span class="error-form" id="error_email"></span>
										</div> <!-- fin item-register -->


										
										
										<div class="item-register col-sm-4 col-xs-12">
											<label class="label-gen label-form">Teléfono:</label>
											<input class="inp-gen inp-form" type="text" name="telefono" id="telefono" placeholder="Ingresa tu teléfono o celular" maxlength="9" onkeypress="return Event_SoloNumeros(event);" required minlength="7">
											<span class="error-form" id="error_telefono"></span>
										</div> <!-- fin item-register -->


										<div class="item-register col-sm-4 col-xs-12">
											<label class="label-gen label-form">Sexo:</label>
											<select class="select-gen select-form" id="sexo" name="sexo" required>
												<option value="">Selecciona tu sexo</option>
												<option value="F">Femenino</option>
												<option value="M">Masculino</option>
											</select>
											<span class="error-form" id="error_sexo"></span>
										</div> <!-- fin item-register -->



										<div class="item-register col-sm-8">
											<label class="label-gen label-form">Fecha de Nacimiento:</label>
											<div class="date-register">
												<select class="select-gen select-form select-form-day" name="dia" id="dia" required>
													<option value="">DD</option>
						                            <option value="1">1</option>
						                            <option value="2">2</option>
						                            <option value="3">3</option>
						                            <option value="4">4</option>
						                            <option value="5">5</option>
						                            <option value="6">6</option>
						                            <option value="7">7</option>
						                            <option value="8">8</option>
						                            <option value="9">9</option>
						                            <option value="10">10</option>
						                            <option value="11">11</option>
						                            <option value="12">12</option>
						                            <option value="13">13</option>
						                            <option value="14">14</option>
						                            <option value="15">15</option>
						                            <option value="16">16</option>
						                            <option value="17">17</option>
						                            <option value="18">18</option>
						                            <option value="19">19</option>
						                            <option value="20">20</option>
						                            <option value="21">21</option>
						                            <option value="22">22</option>
						                            <option value="23">23</option>
						                            <option value="24">24</option>
						                            <option value="25">25</option>
						                            <option value="26">26</option>
						                            <option value="27">27</option>
						                            <option value="28">28</option>
						                            <option value="29">29</option>
						                            <option value="30">30</option>
						                            <option value="31">31</option>
												</select>

												<select class="select-gen select-form select-form-month" name="mes" id="mes" required>
													<option value="">MM</option>
													<option value="01">Enero</option>
													<option value="02">Febrero</option>
													<option value="03">Marzo</option>
													<option value="04">Abril</option>
													<option value="05">Mayo</option>
													<option value="06">Junio</option>
													<option value="07">Julio</option>
													<option value="08">Agosto</option>
													<option value="09">Septiembre</option>
													<option value="10">Octubre</option>
													<option value="11">Noviembre</option>
													<option value="12">Diciembre</option>
												</select>


												<select class="select-gen select-form select-form-year" name="anio" id="anio" required>
													<option value="">AA</option>
													<option value="2012">2012</option>
													<option value="2011">2011</option>
													<option value="2010">2010</option>
													<option value="2009">2009</option>
													<option value="2008">2008</option>
													<option value="2007">2007</option>
						                            <option value="2006">2006</option>
						                            <option value="2005">2005</option>
						                            <option value="2004">2004</option>
						                            <option value="2003">2003</option>
						                            <option value="2002">2002</option>
						                            <option value="2001">2001</option>
						                            <option value="2000">2000</option>
						                            <option value="1999">1999</option>
						                            <option value="1998">1998</option>
						                            <option value="1997">1997</option>
						                            <option value="1996">1996</option>
						                            <option value="1995">1995</option>
						                            <option value="1994">1994</option>
						                            <option value="1993">1993</option>
						                            <option value="1992">1992</option>
						                            <option value="1991">1991</option>
						                            <option value="1990">1990</option>
						                            <option value="1989">1989</option>
						                            <option value="1988">1988</option>
						                            <option value="1987">1987</option>
						                            <option value="1986">1986</option>
						                            <option value="1985">1985</option>
						                            <option value="1984">1984</option>
						                            <option value="1983">1983</option>
						                            <option value="1982">1982</option>
						                            <option value="1981">1981</option>
						                            <option value="1980">1980</option>
						                            <option value="1979">1979</option>
						                            <option value="1978">1978</option>
						                            <option value="1977">1977</option>
						                            <option value="1976">1976</option>
						                            <option value="1975">1975</option>
						                            <option value="1974">1974</option>
						                            <option value="1973">1973</option>
						                            <option value="1972">1972</option>
						                            <option value="1971">1971</option>
						                            <option value="1970">1970</option>
						                            <option value="1969">1969</option>
						                            <option value="1968">1968</option>
						                            <option value="1967">1967</option>
						                            <option value="1966">1966</option>
						                            <option value="1965">1965</option>
						                            <option value="1964">1964</option>
						                            <option value="1963">1963</option>
						                            <option value="1962">1962</option>
						                            <option value="1961">1961</option>
						                            <option value="1960">1960</option>
						                            <option value="1959">1959</option>
						                            <option value="1958">1958</option>
						                            <option value="1957">1957</option>
						                            <option value="1956">1956</option>
						                            <option value="1955">1955</option>
						                            <option value="1954">1954</option>
						                            <option value="1953">1953</option>
						                            <option value="1952">1952</option>
						                            <option value="1951">1951</option>
						                            <option value="1950">1950</option>
						                            <option value="1949">1949</option>
						                            <option value="1948">1948</option>
						                            <option value="1947">1947</option>
						                            <option value="1946">1946</option>
						                            <option value="1945">1945</option>
						                            <option value="1944">1944</option>
						                            <option value="1943">1943</option>
						                            <option value="1942">1942</option>
						                            <option value="1941">1941</option>
						                            <option value="1940">1940</option>
						                            <option value="1939">1939</option>
						                            <option value="1938">1938</option>
						                            <option value="1937">1937</option>
						                            <option value="1936">1936</option>
						                            <option value="1935">1935</option>
						                            <option value="1934">1934</option>
						                            <option value="1933">1933</option>
						                            <option value="1932">1932</option>
						                            <option value="1931">1931</option>
						                            <option value="1930">1930</option>
												</select>
											</div> <!-- fin date-register -->
											<span class="error-form" id="error_nacimiento"></span>
										</div> <!-- fin item-registro -->




										<div class="item-register col-sm-4">
											<label class="label-gen label-form">Distrito:</label>
											<select class="select-gen select-form" name="departamento" id="departamento" required>
												<option value="">Ingresa tu departamento</option>
												<option value="1094">Lima</option>
												<option value="484">Callao</option>
											</select>
											<span class="error-form" id="error_departamento"></span>
										</div> <!-- fin item-register -->

										
									</div> <!-- fin overflow -->

									
									<div class="check-group col-md-12">
										<div class="checkbox">
											<input type="checkbox" id="info" name="boletin" value="1">
				        					<label class="label-condicion" for="info">Deseo recibir información de Radio Planeta y sus anunciantes.</label>
				                    	</div>
										
										<div class="checkbox">
				                    		<input type="checkbox" value="1" name="tyc" id="tyc">
				                    		<label class="label-condicion" for="tyc">Acepto los <a data-toggle="modal" data-target="#TyC">Términos y Condiciones</a></label>
				                    		<span class="error-form" id="error_tyc"></span>
				                    	</div>
									</div>



									<div class="group-btn">
										<button class="btn-gen button-gen-send animated infinite pulse" type="submit" onclick="ga('send', 'event', 'Desafío Oh my Gachi', 'Registrarse');">
											<div class="box-shadow-inset"></div>
											<p>enviar</p>
										</button>



										<a class="btn-gen btn-cancel" href="index.php" onclick="ga('send', 'event', 'Desafío Oh my Gachi', 'Cancelar Registro');">
											<div class="box-shadow-inset"></div>
											<p>Cancelar</p>
										</a>	

										
									</div>
									
								</form>

							</div> <!-- fin wrapp-register -->

						</div> <!-- fin content-register-in -->

					</div>

				</div> <!-- fin container -->


		</article> <!-- fin content-register -->

	</section> <!-- fin core-wrapp -->
	
	

	<?php include ('application/templates/footer.php');?>


	<div class="wrapper-modal-gen">
	    <div class="modalexiste modal fade" id="gracias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

		    <div class="modal-dialog" role="document">
		    	<div class="content-box">
		        	<?php include ('application/modals/gracias.php');?>
		        </div>
		    </div>
	    </div>
    </div> <!-- fin wrapper-modal-gen -->
	

	<!-- Modal -->
    <div class="wrapper-modal-gen">
	    <div class="modalexiste modal fade" id="existe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

		    <div class="modal-dialog" role="document">
		    	<div class="content-box">
		        	<?php include ('application/modals/existe.php');?>
		        </div>
		    </div>
	    </div>
    </div> <!-- fin wrapper-modal-gen -->

 <?php 
if(!empty($_GET['msg'])){	
	$msg = $_GET['msg'];
	
	if($msg == 'existe'){ ?>
			<script type="text/javascript">
	          $('#existe').modal('show');
	        </script>
	<?php 
	}
}
?>
</body>
</html>