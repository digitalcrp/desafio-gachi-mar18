<?php
// setlocale(LC_ALL, 'es_ES');
require_once("application/config/conn.php");
require_once("application/config/tools.php");
require_once("application/config/emblue.php");
require_once("application/config/encode.php");
session_start();


$tool = new Tools();

$action = $_POST['action'];
$campania = 376;
$fecreg = date("Y-m-d");
$horreg = date("H:i:s");
$fecha = date("Y-m-d H:i:s");


switch ($action) {

    case "login":
        $email = $_POST['email'];

        if(!empty($email))
        {
            // Valida si el usuario (email) existe en la BD
            $id_inscrito = $tool->valEmail($email, $campania);
            $puntaje_inscrito = 0;

            // el usuario ya existe    
            if ($id_inscrito > 0) 
            {    
                $nombre_inscrito = $tool->getNombres($id_inscrito);
                $puntaje_inscrito = $tool->getPuntaje($id_inscrito);
                // die($puntaje_inscrito);

                // Guardo su nombre y ID en sesiones
                $_SESSION['idins_gachi'] = $id_inscrito;
                $_SESSION['nomins_gachi'] =  $nombre_inscrito;
                $_SESSION['ptjein_gachi'] =  $puntaje_inscrito;
                // die($puntaje_inscrito);

                // Etapa
                $etapa = 2;
                if($fecreg == '2018-04-04')
                {
                        if($horreg == '11:00:00')
                        {
                            $etapa = 3;
                        }
                }else
                {
                    $etapa = 2;
                }

                // // Obtengo su código de interaccion
                // $tool->getInteraccion($campania, $id_inscrito, $fecreg, $etapa);
                // $id_interaccion = NULL;
                // $cantidad = NULL;

                // if(!empty($_SESSION['idint_gachi']))
                // {
                //     $id_interaccion = $_SESSION['idint_gachi'];
                // }

                // if(!empty($_SESSION['cantidad_gachi']))
                // {
                //     $cantidad = $_SESSION['cantidad_gachi']; 
                // }       
               
                // if(empty($cantidad))
                // {
                //     $_SESSION['estado'] = 1;
                //     unset($_SESSION['jugadas_inscrito'][1]);
                //     unset($_SESSION['resultados_inscrito'][1]);
                //     unset($_SESSION['pregunta_inscrito'][1]);

                //     unset($_SESSION['jugadas_inscrito'][2]);
                //     unset($_SESSION['resultados_inscrito'][2]);
                //     unset($_SESSION['pregunta_inscrito'][2]);

                //     header('Location: trivia1.php');
                // }   
                // else{
                //     $_SESSION['estado'] = 2;
                //     header('Location: index.php');
                // }                      
                header('Location: trivia2.php');
            }
            else
            {
           
                $param['email'] = $email;
                $param = encode_this(serialize($param));
                $_SESSION['datos_inscrito'] = $param;

                header('Location: registro.php');
            }

        }
        else
        {
            header('Location: index.php');
            
        }
    break;

    case "registro":
        //Variables
        $nombres = $_POST['nombres'];
        $apellidos = $_POST['apellidos'];   
        $telefono = $_POST['telefono'];       
        $dni = $_POST['dni'];        
        $email = $_POST['email'];
        $departamento = $_POST['departamento'];
        $genero = $_POST['sexo'];
        $dia = $_POST['dia'];
        $mes = $_POST['mes'];
        $anio = $_POST['anio'];
        $fechayhora = mktime(0, 0, 0, $mes, $dia, $anio);
        $fechayhora = date('Y-m-d h:i:s', $fechayhora);
   
        if(empty($_POST['boletin']))
        {
            $boletin = 0;
        }
        else
        {
            $boletin = 1;
        }

        //Validar Campos
        if(!empty($nombres) && !empty($apellidos) && !empty($telefono) && !empty($dni))
        {
            if(!empty($email) && !empty($dni))
            {
                $id_inscrito = $tool->valInsrito($dni, $email, $campania);

                if ($id_inscrito == 0) 
                {
                    $inscrito['nombre'] = mysql_real_escape_string(utf8_decode($nombres));
                    $inscrito['ap_paterno'] = mysql_real_escape_string(utf8_decode($apellidos));
                    $inscrito['dni'] = mysql_real_escape_string($dni);
                    $inscrito['email'] = mysql_real_escape_string($email);
                    $inscrito['telefono'] = mysql_real_escape_string($telefono);
                    $inscrito['fecha_registro'] = $fecreg;
                    $inscrito['hora_registro'] = $horreg;
                    $inscrito['ubigeo_id'] = $departamento;
                    $inscrito['genero'] = $genero;
                    $inscrito['fecha_nacimiento'] = $fechayhora;
                    $inscrito['campania_id'] = mysql_real_escape_string($campania);
                    $inscrito['acepto_boletin'] = mysql_real_escape_string($boletin);
                    $inscrito['acepto_tyc'] = 1;
                    $inscrito['estado'] = 1;
                    $inscrito['puntaje'] = 1;
                    $id_inscrito = $tool->insert("inscritos", $inscrito);

                    $_SESSION['idins_gachi'] = $id_inscrito;
                    $_SESSION['nomins_gachi'] =  $nombres;
                    $_SESSION['ptjein_gachi'] =  1;

                    $emblue = new Emblue();

                    if ($boletin == 1) 
                    {
                       // Emblue
                       
                        $user_emblue = $emblue->addContact($nombres, $email, $apellidos, $telefono, $dni, $genero, $fechayhora);

                        $datos_emblue = array(
                            'id_inscrito' => $id_inscrito,
                            'id_campania' => $campania,
                            'id_emblue' => $user_emblue->EmailId,
                            'estado' => $user_emblue->Description,
                            'respuesta' => $user_emblue->ContactoEstadoId
                        );
                        $tool->insert("inscritos_detalle", $datos_emblue);
                    }

                    $_SESSION['estado']  = 1;
                    header('Location: index.php?msg=gracias');  
                }
                else
                {                   
                    header("Location: registro.php?msg=existe");  
                }
            }
        }
        else
        {
           header("Location: index.php");
        }
    break;

    case "votar":
        $id_frase = $_POST['id_frase'];
        $fecha = $_POST['fecha'];
        $id_pregunta = $_POST['id_pregunta'];
        $ptje = $_POST['ptje'];  
        $subcategoria_id = $_POST['subcategoria_id'];   

        $txt_r = $_POST['txt_r'];
        $txt_c = $_POST['txt_c'];
        $txt_p = $_POST['txt_p'];

        if(empty($_SESSION['jugadas_inscrito'][$subcategoria_id]))
        {
            $jugadas = array();
            $resultados = array();
        }
        else
        {
            $jugadas = unserialize($_SESSION['jugadas_inscrito'][$subcategoria_id]);
            $resultados = unserialize($_SESSION['resultados_inscrito'][$subcategoria_id]);
        }

        
        $valor = array( 'id_pregunta' => $id_pregunta, 'id_frase' => $id_frase, 'ptje' => $ptje);
        array_push ( $jugadas , $valor );

        $valor2 = array( 'id_pregunta' => $id_pregunta, 'txt_c' => $txt_c, 'txt_r' => $txt_r,'ptje' => $ptje, 'txt_p' => $txt_p);
        array_push ( $resultados , $valor2 );

        // var_dump($resultados);


        $_SESSION['jugadas_inscrito'][$subcategoria_id] = serialize($jugadas);
        $_SESSION['resultados_inscrito'][$subcategoria_id] = serialize($resultados);
        // $_SESSION['pregunta_inscrito'] = $id_pregunta + 1;
       

        if($id_frase >= 0 && $id_frase <=5)
        {
               $id_inscrito = $_SESSION['idins_gachi'];

                //Interaccion de usuario
                if(!empty($_SESSION['idint_gachi']))
                {
                    $id_interaccion = $_SESSION['idint_gachi']; 
                }
                
                if(!empty($_SESSION['cantidad_gachi']))
                {
                    $cantidad = $_SESSION['cantidad_gachi'];
                }
                  
                if(!empty($id_interaccion))
                {
                    $_SESSION['estado'] = 2;
                    echo "limite";
                }
                else
                {
                    // Registar Interacción
                    $cantidad = 1;
                    $id_interaccion = $tool->registrarInteraccion($cantidad, $campania, $id_inscrito,  $fecreg, $horreg, $subcategoria_id);

                    $_SESSION['cantidad_gachi'] = $cantidad;
                    $_SESSION['idint_gachi'] = $id_interaccion;
                    $ptje = 0;
                    $x = 0;

                        foreach ($jugadas as $jugada => $value) {
                            // Registrar voto en la BD
                            $tool->registrarVoto($id_inscrito, $campania, $value['id_frase'], $value['id_pregunta'], $fecreg, $horreg, $subcategoria_id);

                            //Aumentar puntaje
                            $tool->registrarRespuesta($id_inscrito, $campania, $value['id_frase'], $fecreg, $horreg, $value['id_pregunta'], $subcategoria_id, $value['ptje'], decode_this($resultados[$x]['txt_r']), decode_this($resultados[$x]['txt_p']));

                            //Acumular puntaje
                            $ptje += $value['ptje'];
                            $x++;
                            
                        }

                    $tool->acumularPuntaje($id_inscrito, $ptje);
                    $_SESSION['ptjein_gachi'] = $_SESSION['ptjein_gachi'] + $ptje;

                    $_SESSION['estado'] = 0;
                    echo "voto-exitoso";         
                }                
           
        }
        else
        {
           header("Location: index.php");
        }

        break;

    case "pregunta":
        $id_frase = $_POST['id_frase'];
        $fecha = $_POST['fecha'];
        $id_pregunta = $_POST['id_pregunta'];
        $ptje = $_POST['ptje'];
        $txt_r = $_POST['txt_r'];
        $txt_c = $_POST['txt_c'];
        $txt_p = $_POST['txt_p'];
        $subcategoria_id = $_POST['subcategoria_id'];   

        if(empty($_SESSION['jugadas_inscrito'][$subcategoria_id]))
        {
            $datos = array();
            $resultados = array();
            $_SESSION['resultados_inscrito'][$subcategoria_id] = NULL;
            $_SESSION['jugadas_inscrito'][$subcategoria_id] = NULL;
        }
        else
        {
            $datos = unserialize($_SESSION['jugadas_inscrito'][$subcategoria_id]);
            $resultados = unserialize($_SESSION['resultados_inscrito'][$subcategoria_id]);
        }
        // echo $subcategoria_id;
        // die($subcategoria_id);
        
        $valor = array( 'id_pregunta' => $id_pregunta, 'id_frase' => $id_frase, 'ptje' => $ptje);
        array_push ( $datos , $valor );

        $valor2 = array( 'id_pregunta' => $id_pregunta, 'txt_c' => $txt_c, 'txt_r' => $txt_r,'ptje' => $ptje, 'txt_p' => $txt_p);
        array_push ( $resultados , $valor2 );


        $_SESSION['jugadas_inscrito'][$subcategoria_id] = serialize($datos);
        $_SESSION['resultados_inscrito'][$subcategoria_id] = serialize($resultados);
        $_SESSION['pregunta_inscrito'][$subcategoria_id] = $id_pregunta + 1;
        $_SESSION['estado'] = 1;

        if($subcategoria_id == 1)
        {
            include("main.php"); 
        }
        else
        {
            include("main_video.php"); 
        }
        

    break;

    case "pregunta_3":
        $files_name = $_POST['files_name'];
        $info = new SplFileInfo($files_name);
        $extension = $info->getExtension();
        $patron = "%\.(jpe?g|png)$%i"; 
        $files_src = 'files/'.$files_name;
        $file_name_se = $info->getBasename('.'.$extension);
        
        $id_inscrito = $_SESSION['idins_gachi'];

        $tool->getInteraccionEtapa3($campania, $id_inscrito, $fecreg, 3);

        //Interaccion de usuario
        if(!empty($_SESSION['idint_gachi']))
        {
            $id_interaccion = $_SESSION['idint_gachi']; 
        }
        
        if(!empty($_SESSION['cantidad_gachi']))
        {
            $cantidad = $_SESSION['cantidad_gachi'];
        }
          
        if(!empty($id_interaccion))
        {
            $_SESSION['estado'] = 2;
            echo "limite";
        }
        else
        {

            if(preg_match($patron, $files_name) == 1)
            {
                if (file_exists($files_src)) {
                    $files_size = filesize($files_src);

                    if ($files_size >= 0 && $files_size < 3000000)
                    {

                        $cantidad = 1;
                        $_SESSION['cantidad_gachi'] = $cantidad;
                        $id_interaccion = $tool->registrarInteraccion($cantidad, $campania, $id_inscrito,  $fecreg, $horreg, 3);

                        $extension = '.'.$extension;
                        $rand = substr(md5(microtime()),rand(0,26),5);
                        $new_name = strtolower(preg_replace('/[^a-zA-Z0-9\']/', '_', $file_name_se.'-'.$rand)).$extension;

                        switch ($extension) {
                            case '.jpeg':
                            case '.jpg':
                                $mime = 'image/jpeg';
                                break;
                            case '.png':
                                $mime = 'image/png';
                                break;
                        }

                        // SUbir Archivo al S3
                        require('aws/aws-autoloader.php');
                        // use Aws\S3\S3Client;

                        $key = 'AKIAI6T7APKGKEDEP27A';
                        $secret_key = '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';
                        $bucket = 'sss.concursos.crp.pe';

                        $sharedConfig = [
                            'region' => 'us-east-1',
                            'version' => 'latest'
                        ];

                        // Create an SDK class used to share configuration across clients
                        $sdk = new Aws\Sdk($sharedConfig);

                        // Create an Amazon S3 client using shared configuration data
                        $credentials = new Aws\Credentials\Credentials($key, $secret_key);
                        $client = $sdk->createS3(['credentials' => $credentials, 'http' => ['verify' => false]]);
                        $mime = 'jpg';
                   
                        // Upload a file.
                        $promise = $client->putObjectAsync(array(
                            'Bucket'       => $bucket,
                            'Key'          => 'planeta/desafio-gachi-mar18/'.$new_name,
                            'SourceFile'   => $files_src,
                            'ContentType'  => $mime,
                            'ACL'          => 'public-read',
                            'StorageClass' => 'STANDARD'
                        ));

                        $result = $promise->wait();

                        // Eliminar de la carpeta 
                        // chmod($files_src,0777);
                        // if(is_file($files_src)) {
                        //    unlink($files_src);
                        // }

                        // Guardar en la BD
                        // Aumentar puntaje
                        $ptje = $_SESSION['ptjein_gachi'] + 50;
                        $_SESSION['ptjein_gachi'] = $ptje;
                        $tool->acumularPuntaje($id_inscrito, $ptje);

                        $files_new_src = 'http://d64z79rfpeif.cloudfront.net/planeta/desafio-gachi-mar18/'.$new_name;
                        $tool->registrarRespuesta($id_inscrito, $campania, 1, $fecreg, $horreg, '', 3, 50, $new_name, $files_new_src);

                        $nombre_usuario = $_SESSION['nomins_gachi'];
                        $tool->registrarMultimedia($id_inscrito, $campania,$nombre_usuario, 3, $fecreg, $new_name, $files_new_src);

                        $_SESSION['estado'] = 0;

                        header("Location: trivia3-resultados.php");
                    }
                    else
                    {
                        header("Location: trivia3.php?msg=error-dibujo");
                    }
                    
                }
                
            }
        }

    break;

    case "logout":
        session_unset();
        session_write_close();
        header("Location: index.php");
    break;

    default:
        header("Location: index.php");
    break;
}
?>
